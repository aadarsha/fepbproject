<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
// Route::get('/backend', function () {

//     return view('layouts.backendmaster');

// })->middleware('auth');

Auth::routes();

// FRONT END ROUTE

// Route::get('/home', 'HomeController@index')->name('home');
// Route::get('/', 'FrontController@index')->name('front.index');
Route::get('/index/{lang?}', 'FrontController@index')->name('front.index');
Route::get('/about', 'FrontController@aboutus')->name('front.about');
Route::get('/yojanakaryakram', 'FrontController@yojanakaryakram')->name('front.yojanakaryakram');
Route::get('/media', 'FrontController@media')->name('front.media');
Route::get('/download', 'FrontController@download')->name('front.download');
Route::get('/pratibedhantype/{type?}', 'FrontController@pratibedhantype')->name('front.pratibedhantype');
Route::get('/contact', 'FrontController@contact')->name('front.contact');
Route::get('/boardkarya', 'FrontController@boardkarya')->name('front.boardkarya');
Route::get('/boardkaryafinal', 'FrontController@boardkaryafinal')->name('front.boardkaryafinal');
Route::get('/samrachana', 'FrontController@samrachana')->name('front.samrachana');
Route::get('/walefare', 'FrontController@walefare')->name('front.walefare');
Route::get('/walefaredetail/{category_id}', 'FrontController@walefaredetail')->name('front.walefaredetail');
Route::get('/memberdetail/{id}', 'FrontController@memberdetail')->name('front.memberdetail');
Route::get('/rojgarprocess', 'FrontController@rojgarprocess')->name('front.rojgarprocess');
Route::get('/mobileapp', 'FrontController@mobileapp')->name('front.mobileapp');
Route::get('/tathyankabibaran', 'FrontController@tathyankabibaran')->name('front.tathyankabibaran');
Route::get('/chetanamulak', 'FrontController@chetanamulak')->name('front.chetanamulak');
Route::get('/chart', 'FrontController@chart')->name('front.chart');
Route::get('/chart2', 'FrontController@chart2')->name('front.chart2');
Route::get('/chart3', 'FrontController@chart3')->name('front.chart3');
Route::get('/photocollection/{catrgoty}', 'FrontController@photocollection')->name('front.photocollection');
Route::get('/lang/{lang}', 'FrontController@language')->name('front.language');

// FRONT END ROUTE Ends Here

// NAXA

Route::get('geo-map',         ['as'=>'geo-map',           'uses' =>'FrontController@geomap']);
Route::get('insurance',       ['as'=>'insurance',         'uses' =>'FrontController@insurance']);
Route::get('orientation',     ['as'=>'orientation',       'uses' =>'FrontController@orientation']);
Route::get('district',         ['as'=>'district',            'uses' =>'FrontController@district']);
Route::get('datatable',         ['as'=>'datatable',            'uses' =>'FrontController@datatable']);




// NAXA

// MAIL
Route::post('/send/email', 'MailController@mail')->name('send.mail');
Route::get('/write/mail', 'MailController@writemail');
Route::post('subscription','MailController@subscription')->name('front.subscription');

// MAIL End Here+

// DEtail PAge
// Route::get('/detail/{busno}/{reservation}/{resnum}','frontController@busseat')->name('bus.busseat');
Route::get('/detail/{id}/{catrgoty}','FrontController@detailpage')->name('front.detailpage');
// DEtail PAge

Route::post('contactstore','FrontController@contactstore')->name('frontcontact.store');
Route::post('gunasostore','FrontController@gunasostore')->name('front.gunasostore');


Route::group(['prefix' => 'backend',  'middleware' => 'auth'], function()
{
    Route::resource('product_category','ProductCategoryController');
    Route::resource('setting','SettingController');
    Route::resource('slider','SliderController');
    Route::resource('about','AboutController');
    Route::resource('general','GeneralController');
    Route::resource('description','DescriptionController');
    Route::post('description_update','DescriptionController@updatestore')->name('description.updatestore');

    Route::resource('chart','ChartController');
    Route::post('chart_update','ChartController@updatestore')->name('chart.updatestore');
    Route::resource('manualTable','ManualTableController');
    Route::resource('tableEntry','TableEntryController');
    Route::resource('janaGunaso','JanaGunasoController');

    Route::resource('boardWork','BoardWorkController');
    Route::get('boardGathan/create','BoardWorkController@creategathan')->name('boardWork.creategathan');
    Route::get('board/index/{category}','BoardWorkController@boardindex');
    Route::resource('boardSachibalaya','BoardSachibalayaController');



    // this is extra page for blog
    Route::resource('blog','BlogController');
    Route::get('media_index/{category}','BlogController@mediaindex');

    Route::get('suchana/create','BlogController@suchana')->name('suchana.create');
    Route::get('press/create','BlogController@press')->name('press.create');
    Route::get('gallery/create','BlogController@gallery')->name('gallery.create');
    Route::get('video/create','BlogController@video')->name('video.create');
    Route::get('bill/create','BlogController@bill')->name('bill.create');
      // this is extra page for blog

    Route::resource('contact','ContactController');
    Route::resource('subscription','SubscriptionController');
    
    Route::resource('openCountry','OpenCountryController');
    Route::resource('neyog','NeyogController');
    Route::resource('bank','BankController');


    Route::resource('mobileApp','MobileAppController');
    Route::resource('banner','BannerController');
    Route::resource('simple','SimpleController');
    Route::resource('importantLink','ImportantLinkController');
    Route::resource('multimedia','MultimediaController');


    Route::get('multimediavideo/create','MultimediaController@multimediavideo')->name('multimediavideo.create');
    Route::get('multimediaaudio/create','MultimediaController@multimediaaudio')->name('multimediaaudio.create');
    Route::get('multimediaphoto/create','MultimediaController@multimediaphoto')->name('multimediaphoto.create');
    Route::get('basicnote/create','MultimediaController@basicnote')->name('basicnote.create');
    Route::get('faq/create','MultimediaController@faq')->name('faq.create');
    Route::get('institute/create','MultimediaController@institute')->name('institute.create');


    Route::get('chetanamulak/{category}','MultimediaController@chetanaindex');

    Route::resource('training','TrainingController');
    Route::resource('extra','ExtraController');
    Route::get('download/{category}','ExtraController@downloadindex');

    Route::get('ain/create','ExtraController@ain')->name('ain.create');

    Route::get('bulletin/create','ExtraController@bulletin')->name('bulletin.create');
    Route::get('prakashan/create','ExtraController@prakashan')->name('prakashan.create');
    Route::get('pratibedhan/create','ExtraController@pratibedhan')->name('pratibedhan.create');
    Route::get('report/create','ExtraController@report')->name('report.create');
    Route::get('other/create','ExtraController@other')->name('other.create');
    Route::get('nebadan/create','ExtraController@nebadan')->name('nebadan.create');

    Route::resource('yojanaKaryakram','YojanaKaryakramController');
    Route::get('yojanaandkaryakram/index/{category}','YojanaKaryakramController@yojanaindex');

    Route::get('yojana/karyakram','YojanaKaryakramController@createkaryakram')->name('yojanaKaryakram.karyakram');
    Route::resource('member','MemberController');
    Route::resource('writeMesage','WriteMesageController');
    Route::resource('frontmember','FrontMemberController');
    Route::get('create_category/{type}','ProductCategoryController@createcategory')->name('createcategory.create');
    Route::get('category_index/{type}','ProductCategoryController@categoryindex')->name('createcategory.index');
});

Route::get('importExport', 'ExcelFileController@importExport')->name('excelindex');
Route::get('downloadExcel/{type}', 'ExcelFileController@downloadExcel');
Route::post('importExcel', 'ExcelFileController@importExcel');
// Route::get('/excel', 'ExcelFileController@index')->name('excelindex');
Route::post('import', 'ExcelFileController@import')->name('import');

// <a href="{{route('bus.busseat',[$singleRow->bus_no,$departDate,$resnum])}}" data-toggle="tooltip" title="Select Seat"><button>Select Seat</button></a></td>
//  <a href="{{url('/front/busseat/'.$singleRow->bus_no.'/'.$departDate.'/'.$resnum)}}" data-toggle="tooltip" title="Select Seat"><button>Select Seat</button></a></td>  --}}
