<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateManualTablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manual_tables', function (Blueprint $table) {
            $table->increments('id');
            $table->text('title')->nullable();
            $table->text ('table_name')->nullable();
            $table->integer('general_id')->unsigned()->nullable();
            $table->text('description')->nullable();
            $table->foreign('general_id')->references('id')->on('generals')->onUpdate('cascade')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('manual_tables');
    }
}
