@extends('layouts.frontmaster')
@push('css')
<link rel="stylesheet" type="text/css" href="{{asset('assets/front/css/magnific-popup.css')}}">

@endpush
@push('js')
<script src="{{asset('assets/front/js/jquery.magnific-popup.min.js')}}"></script>

<script>
    $(window).on('load', function() {
        var url = window.location.href;

        var words = url.split('#');
        var check=words[1];
        {{--  alert(check);  --}}
        if(check=='news')
        {

            $('#news').addClass('in active');
            $('#news1').addClass('in active');
            {{--  $('.checkactive4').removeClass('in active');  --}}
            {{--  $("#hownav").css("display", "inline");  --}}



        }
        if(check=='press')
        {
            $('#press').addClass('in active');
            $('#press1').addClass('in active');
            {{--  $("#whatnav").css("display", "inline");  --}}

        }
        if(check=='video')
        {
            $('#video').addClass('in active');
            $('#video1').addClass('in active');
            {{--  $("#missionnav").css("display", "inline");  --}}

        }
        if(check=='image')
        {
            $('#phuto').addClass('in active');
            $('#phuto1').addClass('in active');

        }

        })
</script>

@endpush
@push('css')
<link rel="stylesheet" type="text/css" href="{{asset('assets/front/css/gallery.css')}}">
{{--  <link rel="stylesheet" type="text/css" href="{{asset('assets/front/css/news.css')}}">  --}}
{{--  <link rel="stylesheet" href="{{asset('assets/front/css/video.css')}}">  --}}

@endpush
@section('content')
<div class="pb-breadcrumb" style="background:url({{asset('assets/front/images/bg/1.jpg')}});">
    <div class="breadcrumb-wrap">
        {{--  <h2>Follow steps</h2>  --}}

        <ul>
                <li><a href="{{route('front.index')}}">गृहपृष्ठ</a></li>
                <?php $segments = ''; ?>  @foreach(Request::segments() as $segment) <?php $segments .= '/'.$segment; ?>        <li>
                        <a href="{{ $segments }}">{{$segment}}</a>
                    </li>
                @endforeach
        </ul>
    </div>
</div>
<div class="inner-page section">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-lg-3">
                    <div class="side-menu">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link " href="#news" id="news1" role="tab" data-toggle="tab">सूचना तथा समाचारहरु</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="#press" id="press1" role="tab" data-toggle="tab">प्रेस विज्ञप्ति</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#phuto" id="phuto1" role="tab" data-toggle="tab">तस्वीर संग्रह </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#video" id="video1" role="tab" data-toggle="tab">भिडीयो संग्रह</a>
                            </li>

                        </ul>
                    </div>
                </div>
                <div class="col-md-9 col-lg-9">
                    <div class="menu-content">
                        <div class="card">
                            <div class="card-body">

                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane" id="news">
                                        <div class="custom-accordion">
                                            <div class="card-header">
                                                <h4>सूचना तथा समाचारहरु</h4>
                                            </div>
                                            <div class="content-list">
                                                <div class="news-content">
                                                    @foreach ($suchanas as $item)
                                                    <div class="news-list">
                                                            <div class="news-thumb">
                                                                <img src="{{asset('/blog_upload/'.$item->image)}}" data-id="SPVccuO9qbw" alt="video-thumb">
                                                            </div>
                                                            <div class="news-text">
                                                                <span>{{$item->date}}</span>
                                                                <h4>
                                                                    <a href="#">{{$item->title}}</a>
                                                                </h4>
                                                                <div class="read-btn">
                                                                    <a href="{{route('front.detailpage',[$item->id,$item->category])}}" class="btn">थप पढ्नुहोस्</a>
                                                                </div>
                                                            </div>

                                                        </div>

                                                    @endforeach

                                                    {{--  <div class="news-list">
                                                        <div class="news-thumb">
                                                            <img src="{{asset('assets/front/images/news/news1.jpg' )}}" data-id="CG93GHxUNlw">
                                                        </div>
                                                        <div class="news-text">
                                                            <span>कात्तिक १०, २०७५</span>
                                                            <h4>
                                                                <a href="#">विदेशमा काम गरी फर्किएर स्वदेशमा उद्यम गरी बसेका उद्यमीलाई राष्ट्रिय सम्मान तथा पुरस्कार
                                                                    व्यवस्थापन कार्य्विधी</a>
                                                            </h4>
                                                            <div class="read-btn">
                                                                <a href="#" class="btn">थप पढ्नुहोस्</a>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="news-list">
                                                        <div class="news-thumb">
                                                            <img src="{{asset('assets/front/images/news/news1.jpg' )}}" data-id="SPVccuO9qbw" alt="video-thumb">
                                                        </div>
                                                        <div class="news-text">
                                                            <span>कात्तिक १०, २०७५</span>
                                                            <h4>
                                                                <a href="#">आ.व.२०७४र०७५ मा वैदेशिक रोजगारीको क्रममा अंगभंगरविरामी हुनेको देशगत र जिल्लागत विवरण</a>
                                                            </h4>
                                                            <div class="read-btn">
                                                                <a href="#" class="btn">थप पढ्नुहोस्</a>
                                                            </div>
                                                        </div>

                                                    </div>  --}}

                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <div role="tabpanel" class="tab-pane " id="press">
                                        <div class="card-header">
                                            <h4>प्रेस विज्ञप्ति
                                            </h4>
                                        </div>
                                        <div class="content-list">
                                            <div class="table table-responsive">
                                                <table class="table table-bordered table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>क्र.स.</th>
                                                            <th>विवरण</th>
                                                            <th>प्रकाशित मिति</th>
                                                            <th class="action-td">डाउनलोड</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php($i=1)
                                                        @foreach ($presses as $item)
                                                        <tr>
                                                                <td>{{$i++}}</td>
                                                                <td>{{$item->title}}
                                                                </td>
                                                                <td>30-10-2018</td>
                                                                <td class="action-td">
                                                                    <a href="{{asset('/blog_upload/'.$item->other_file)}}" download data-toggle="tooltip" data-placement="top" title="" data-original-title="Download">
                                                                        <i class="fa fa-download"></i>
                                                                    </a>
                                                                    <a href="{{asset('/blog_upload/'.$item->other_file)}}" target="_blank" data-toggle="tooltip" data-placement="top" title="" data-original-title="View">
                                                                        <i class="fa fa-eye"></i>
                                                                    </a>

                                                                </td>
                                                            </tr>

                                                        @endforeach


                                                        {{--  <tr>
                                                            <td>2</td>
                                                            <td>न्यूनतम पारिश्रमिक

                                                            </td>
                                                            <td>9-19-2018</td>
                                                            <td class="action-td">
                                                                <a href="../folder/workers relitives.pdf" data-toggle="tooltip" data-placement="top" title="" data-original-title="Download">
                                                                    <i class="fa fa-download"></i>
                                                                </a>
                                                                <a href="../folder/workers relitives.pdf" data-toggle="tooltip" data-placement="top" title="" data-original-title="View">
                                                                    <i class="fa fa-eye"></i>
                                                                </a>

                                                            </td>
                                                        </tr>  --}}
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>


                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="phuto">
                                        <div class="card-header">
                                            <h4>तस्वीर संग्रह
                                            </h4>
                                        </div>
                                        <div class="container galleryWrapper">

                                            <div class="row">


                                                    @foreach ($photos->unique('title') as $item)


                                                    <div class="col-lg-4 col-md-6 thumb">
                                                        <div class="galleryBox">
                                                            {{--  <a class="thumbnail" href="{{route('front.photocollection',[$item->title])}}"  data-toggle="modal"  data-target='#imagegallery{{$j}}'>  --}}
                                                            <a class="thumbnail" href="{{route('front.photocollection',[$item->title])}}">
                                                                <img class="img-thumbnail" src="{{asset('/blog_upload/'.$item->image)}}" alt="Another alt text">
                                                                <div class="content">
                                                                    <h3>{{$item->title}}</h3>

                                                                </div>
                                                            </a>
                                                        </div>
                                                    </div>


                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane " id="video">
                                        <div class="card-header">
                                            <h4>भिडीयो संग्रह
                                            </h4>
                                        </div>
                                        <!-- video -->
                                        <section class="videoWrapper">
                                            <div class="boxes">
                                                @foreach ($videos as $item)
                                                <div class="box">
                                                        <div class="video">
                                                                <a class="video-popup" href="{{$item->short_description}}" data-id="xadQwMU4qtU"><img src="{{asset('/blog_upload/'.$item->image)}}" height="100" width="100" data-id="SPVccuO9qbw" alt="video-thumb"></a>

                                                            {{--  <iframe width="560" height="315" src="{{$item->short_description}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>  --}}
                                                        </div>
                                                        <div class="content">
                                                            <h3>{{$item->title}}</h3>
                                                            <p class="date">{{$item->date}}</p>
                                                            <hr>
                                                            <p class="description">{!!$item->description!!}</p>
                                                        </div>
                                                    </div>

                                                @endforeach

                                                {{--  <div class="box">
                                                    <div class="video">
                                                        <iframe width="560" height="315" src="https://www.youtube.com/embed/0EwXXrZq0kQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                                    </div>
                                                    <div class="content">
                                                        <h3>Compiled Ads on Television</h3>
                                                        <p class="date"></p>Apr 20, 2012</p>
                                                        <hr>
                                                        <p class="description">Description</p>
                                                    </div>
                                                </div>  --}}
                                                {{--  <div class="box">
                                                    <div class="video">
                                                        <iframe width="560" height="315" src="https://www.youtube.com/embed/cx7ifeegzqU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                                    </div>
                                                    <div class="content">
                                                        <h3>Insurance before you go for foreign employement</h3>
                                                        <p class="date">Apr 19, 2012</p>
                                                        <hr>
                                                        <p class="description">Description</p>
                                                    </div>
                                                </div>  --}}
                                                {{--  <div class="box">
                                                    <div class="video">
                                                        <iframe width="560" height="315" src="https://www.youtube.com/embed/qvTDLTvptpU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                                    </div>
                                                    <div class="content">
                                                        <h3>Don't Stay longer to the working country then the Visa</h3>
                                                        <p class="date">Apr 19, 2012</p>
                                                        <hr>
                                                        <p class="description">Description</p>
                                                    </div>
                                                </div>  --}}
                                                {{--  <div class="box">
                                                    <div class="video">
                                                        <iframe width="560" height="315" src="https://www.youtube.com/embed/UxHikYCD1M4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                                    </div>
                                                    <div class="content">
                                                        <h3>MOEWRWS Awareness</h3>
                                                        <p class="date">Apr 19, 2012</p>
                                                        <hr>
                                                        <p class="description">Description</p>
                                                    </div>
                                                </div>  --}}
                                            </div>

                                        </section>
                                        <!-- video ends here -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>


@endsection
