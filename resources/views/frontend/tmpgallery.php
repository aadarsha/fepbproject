
{{--

<div class="inner-page section">
        <div class="container">
            <div class="row">
                {{--  <div class="col-md-offset-1 col-lg-offset-1 col-md-10 col-lg-10 ">  --}}
                <div class="">
                    <div class="menu-content">
                        <div class="card">
                            <div class="card-body">

                                <div class="tab-content">

                                    <div role="tabpanel">
                                        <div class="card-header">
                                            <h4>तस्वीर संग्रह
                                            </h4>
                                        </div>
                                        <div class="container galleryWrapper">
                                            <div class="row">
                                                {{--  <div class="row">  --}}
                                                    @php($j=1)
                                                    @foreach ($photos as $item)
                                                    <input type="hidden" value="{{$j++}}">

                                                    <div class="col-lg-4 col-md-6 thumb">
                                                        <div class="galleryBox">
                                                            <a class="thumbnail" href="#"  data-toggle="modal"  data-target='#imagegallery{{$j}}'>
                                                                <img class="img-thumbnail" src="{{asset('/blog_upload/'.$item->image)}}" alt="Another alt text">
                                                                <div class="content">
                                                                    <h3>{{$item->short_description}}</h3>

                                                                </div>
                                                            </a>
                                                        </div>
                                                    </div>


  <!-- The Modal -->
                                        <div class="modal fade infoModal" id="imagegallery{{$j}}">
                                            <div class="modal-dialog">
                                            <div class="modal-content">

                                                <!-- Modal Header -->
                                                <div class="modal-header">
                                                <h4 class="modal-title">{{$item->short_description}}</h4>
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                </div>

                                                <!-- Modal body -->
                                                <div class="modal-body">
                                                        <img id="image-gallery-image" class="img-responsive col-md-12" src="{{asset('/blog_upload/'.$item->image)}}">
                                                        <br>
                                                        <p style="padding:10px;">{!!$item->description!!}</p>

                                                </div>

                                                <!-- Modal footer -->
                                                <div class="modal-footer">
                                                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-close"></i></button>
                                                </div>

                                            </div>
                                            </div>
                                        </div>

                                                @endforeach
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

  --}}


<div class="inner-page section">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-lg-12">
                <div class="menu-content">
                    <div class="card">
                        <div class="card-body">

                            <div class="tab-content">
                                <div class="card-header">
                                    <h4>तस्वीर संग्रह</h4>
                                </div>
                                <div class="container infographicsWrapper">
                                    <div class="row">
                                        <div class="row">
                                                @php($j=1)
                                                @foreach ($photos as $item)
                                                <input type="hidden" value="{{$j++}}">

                                            <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                                                <a class="thumbnail major-preview" href="{{asset('/blog_upload/'.$item->image)}}">
                                                    <img class="img-thumbnail" src="{{asset('/blog_upload/'.$item->image)}}" alt="Another alt text">
                                                </a>
                                            </div>
                                            @endforeach

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>