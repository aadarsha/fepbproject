@extends('layouts.frontmaster')

@section('content')
<div class="pb-breadcrumb" style="background:url({{asset('assets/front/images/bg/1.jpg')}});">
    <div class="breadcrumb-wrap">
        {{--  <h2>Follow steps</h2>  --}}

        <ul>
                <li><a href="{{route('front.index')}}">गृहपृष्ठ</a></li>
                <?php $segments = ''; ?>  @foreach(Request::segments() as $segment) <?php $segments .= '/'.$segment; ?>        <li>
                        <a href="{{ $segments }}">{{$segment}}</a>
                    </li>
                @endforeach
        </ul>
    </div>
</div>


    <div class="inner-page section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="menu-content">
                            <div class="card">
                                <div class="card-body">
                                    <div class="organogram">
                                        <ul>
                                            <li class="director org-center">
                                                <div class="org-item">
                                                     @foreach ($members as $item)
                                                @if ($item->rank==0)
                                                <a href="{{route('front.memberdetail',$item->id)}}">
                                                        <div class="orgimg">
                                                            <img src="{{asset('/member_upload/'.$item->image)}}" alt="org">
                                                        </div>
                                                        <div class="content">
                                                            <h4><a href="{{route('front.memberdetail',$item->id)}}">{{$item->name}}</a></h4>
                                                            <span>{{$item->position}}</span>
                                                        </div>
                                                    </a>
                                                    
                                                @endif
                                                    
                                                @endforeach
                                                
                                                </div>

                                                <ul class="assistant">
                                                    @foreach ($members as $ch)
                                                    @if ($ch->rank==1)
                                                    <li class="org-left">
                                                            <div class="org-item">
                                                                    <a href="{{route('front.memberdetail',$ch->id)}}">
                                                                            <div class="orgimg">
                                                                                <img src="{{asset('/member_upload/'.$ch->image)}}" alt="org">
                                                                            </div>
                                                                            <div class="content">
                                                                                <h4><a href="{{route('front.memberdetail',$ch->id)}}">{{$ch->name}}</a></h4>
                                                                                <span>{{$ch->position}}</span>
                                                                            </div>
                                                                        </a>
                                                           
                                                            </div>
                                                            <ul class="sublist">
                                                                @foreach ($members as $child)
                                                                @if ($child->parent==$ch->id)
                                                                <li>
                                                                        <div class="org-item">
                                                                                <a href="{{route('front.memberdetail',$child->id)}}">
                                                                                        <div class="orgimg">
                                                                                            <img src="{{asset('/member_upload/'.$child->image)}}" alt="org">
                                                                                        </div>
                                                                                        <div class="content">
                                                                                            <h4><a href="{{route('front.memberdetail',$child->id)}}">{{$child->name}}</a></h4>
                                                                                            <span>{{$child->position}}</span>
                                                                                        </div>
                                                                                    </a>
                                                                        
                                                                        </div>
                                                                    </li>
                                                                    
                                                                @endif
                                                                    
                                                                @endforeach
                                                               
                                                            </ul>
                                                        </li>
                                                        
                                                        
                                                        @elseif ($ch->rank==2)

                                                        <li>
                                                                <div class="org-item">
                                                                        <a href="{{route('front.memberdetail',$ch->id)}}">
                                                                                <div class="orgimg">
                                                                                    <img src="{{asset('/member_upload/'.$ch->image)}}" alt="org">
                                                                                </div>
                                                                                <div class="content">
                                                                                    <h4><a href="{{route('front.memberdetail',$ch->id)}}">{{$ch->name}}</a></h4>
                                                                                    <span>{{$ch->position}}</span>
                                                                                </div>
                                                                            </a>
                                                                </div>
                                                                <ul class="sublist">
                                                                        @foreach ($members as $child2)
                                                                        @if ($child2->parent==$ch->id)
                                                                    <li>
                                                                        <div class="org-item">
                                                                                <a href="{{route('front.memberdetail',$child2->id)}}">
                                                                                        <div class="orgimg">
                                                                                            <img src="{{asset('/member_upload/'.$child2->image)}}" alt="org">
                                                                                        </div>
                                                                                        <div class="content">
                                                                                            <h4><a href="{{route('front.memberdetail',$child2->id)}}">{{$child2->name}}</a></h4>
                                                                                            <span>{{$child2->position}}</span>
                                                                                        </div>
                                                                                    </a>
                                                                        </div>
                                                                    </li>
                                                                    @endif
                                                                    @endforeach
                                                                   
                                                                    
                                                                </ul>
                                                            </li>
                                                            
                                                            @elseif ($ch->rank==3)
                                                            <li  class="org-right">
                                                                    <div class="org-item">
                                                                            <a href="{{route('front.memberdetail',$ch->id)}}">
                                                                                    <div class="orgimg">
                                                                                        <img src="{{asset('/member_upload/'.$ch->image)}}" alt="org">
                                                                                    </div>
                                                                                    <div class="content">
                                                                                        <h4><a href="{{route('front.memberdetail',$ch->id)}}">{{$ch->name}}</a></h4>
                                                                                        <span>{{$ch->position}}</span>
                                                                                    </div>
                                                                                </a>
                                                                    </div>
                                                                    <ul class="sublist">
                                                                            @foreach ($members as $child3)
                                                                            @if ($child3->parent==$ch->id)
                                                                        <li>
                                                                            <div class="org-item">
                                                                                    <a href="{{route('front.memberdetail',$child3->id)}}">
                                                                                            <div class="orgimg">
                                                                                                <img src="{{asset('/member_upload/'.$child3->image)}}" alt="org">
                                                                                            </div>
                                                                                            <div class="content">
                                                                                                <h4><a href="{{route('front.memberdetail',$child3->id)}}">{{$child3->name}}</a></h4>
                                                                                                <span>{{$child3->position}}</span>
                                                                                            </div>
                                                                                        </a>
                                                                            </div>
                                                                        </li>
                                                                        @endif
                                                                        @endforeach
                                                                      
                                                                    </ul>
                                                                </li>
                                                            
                                                        
                                                        
                                                    @endif
                                                        
                                                   
                                                   
                                                  
                                                   
                                                    @endforeach
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    
@endsection
