@extends('layouts.frontmaster')
@push('css')
<link rel="stylesheet" href="{{asset('assets/front/css/contactus.css')}}">

@endpush
@section('content')
<!-- mainbody start -->
<div class="contactus">
    <div class="map">
        {{--  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3532.6495952385017!2d85.32628731454408!3d27.697222982796116!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39eb19a4634ce48d%3A0x4a421be0363978f2!2sGovernment+Of+Nepal+Ministry+Of+Labour+%26+Employment+Secretarial+Of+Foreign+Employment+Promotion+Board!5e0!3m2!1sen!2snp!4v1541153472204"
        width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>  --}}
        <iframe width="100%" height="450" src="https://maps.google.com/maps?width=100%&amp;height=450&amp;hl=en&amp;coord=27.692555,85.335925&amp;q=Foreign%20Employment%20Promotion%20Board+(Foreign%20Employment%20Promotion%20Board)&amp;ie=UTF8&amp;t=&amp;z=16&amp;iwloc=B&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"><a href="https://www.maps.ie/map-my-route/">Map a route</a></iframe>
    </div>
    <div class="contactus1">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-lg-6 col-sm-12">
                    <!-- add content contact -->
                    <div class="adress">
                        <div class="title">
                            <h3>ठेगाना</h3>
                        </div>
                        <div class="contact-content mrt">
                            <ul>
                                {{--  @foreach ($settings as $setting)  --}}
                                <li><i class="fa fa-map-marker"></i> {{$setting->name}}</li>
                                <li><i class="fa fa-map-marker"></i>{{$setting->address}}</li>
                                <li><i class="fa fa-phone"></i>{{$setting->phone1}}, {{$setting->phone2}}</li>
                                <li><i class="fa fa-fax"></i>{{$setting->fax}}</li>
                                <li><i class="fa fa-phone"></i>{{$setting->tollfree}}</li>
                                <li> <i class="fa fa-envelope"></i>{{$setting->email}}</li>

                                {{--  @endforeach  --}}

                            </ul>
                        </div>
                    </div>
                    <!-- add content contact end-->
                </div>
                <div class="col-md-6 col-lg-6 col-sm-12 col-xl-6 col-xs-12">
                    <div class="form_main">
                        <div class="title">
                            <h3>सम्पर्क</h3>
                            @if(Session::has('success'))
                            <div class="alert alert-success">
                                {{ Session::get('success') }}
                            </div>
                        @endif
                        @if(Session::has('error'))
                            <div class="alert alert-danger">
                                {{ Session::get('error') }}
                            </div>
                        @endif
                        </div>
                        <div class="form">

                                    <form method="post" action="{{route('frontcontact.store')}}"  enctype="multipart/form-data" id="valid_form" name="contactFrm" >
                                        {{csrf_field()}}
                                    <div class="form-group">
                                        <label for="lname"> Your Name
                                            <span>*</span>
                                        </label>
                                        <input type="text" required value="" id="name" name="name" class="txt">
                                    </div>
                                    <div class="form-group"></div>
                                    <label for="lEMAIL">Your Email
                                        <span>*</span>
                                    </label>
                                    <input type="email" required value="" name="email" id="email" class="txt">
                                </div>
                                <div class="form-group">
                                    <label for="lsubject"> Subject</label>
                                    <input type="text" required value="" name="subject" id="subject" class="txt">
                                </div>
                                <div class="form-group">
                                    <label for="lmessage"> Your Message</label>
                                    <textarea name="message" id="message" type="text" class="txt_3" required></textarea>
                                </div>


                                <input type="submit" value="SEND" name="submit" class="txt2">
                            </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<!-- mainbody end -->
@endsection
