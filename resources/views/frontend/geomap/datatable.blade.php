@extends('layouts.frontmaster')
@push('css')

    <link rel="stylesheet" type="text/css" href="{{ asset('frontend/newcss/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend/newcss/dataTables.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend/newcss/publication.css') }}">
    <link href="{{ asset('frontend/newcss/c3.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('frontend/newcss/dataTables-custom.css') }}">
@endpush  
@section('content') 
    <div class="pb-breadcrumb" style="background:url({{asset('assets/front/images/bg/1.jpg')}});">
        <div class="breadcrumb-wrap">
            <h2>DATATABLES</h2>
        </div>
    </div>
    <div class="inner-page section">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-lg-3">
                    <div class="side-menu">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" href="#rules" role="tab" data-toggle="tab">Orientation Centres</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#bulletin" role="tab" data-toggle="tab">Insurance Company </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#pragati" role="tab" data-toggle="tab">Recruiting Agency</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link " href="#publication" role="tab" data-toggle="tab">Medical Centres</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#downloads" role="tab" data-toggle="tab">Migrant Resource Centres</a>
                            </li>

                        </ul>
                    </div>
                </div>
                <div class="col-md-9 col-lg-9">
                    <div class="menu-content">
                        <div class="card">
                            <div class="card-body">

                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane" id="publication">
                                        <div class="custom-accordion">
                                            <div class="card-header">
                                                <h4>Medical Centres</h4>
                                            </div>
                                            <section class="publicationWrapper">
                                              <div class="content-list">

                                                <div class="table table-responsive">

                                                    <table class="table redtbl table-bordered table-hover dataTable" id="insurances">
                                                        <thead>
                                                            <th>ID</th>
                                                            <th>Medical Name</th>
                                                            <th>Address</th>
                                                            <th>Contact</th>
                                                            <th>Email</th>
                                                            <th>Website</th>
                                                            <th>Status</th>
                                                            <th>Remarks</th>
                                                            <th>Latitude</th>
                                                            <th>Longitude</th>
                                                        </thead>
                                                        <tbody>

                                                        </tbody>
                                                    </table>
                                                    <h2 style="text-align: center";> NO DATA AVAILABLE.....</h2>
                                                </div>
                                            </div>  
                                            </section>

                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane " id="pragati">
                                        <div class="card-header">
                                            <h4>Recruiting Agency
                                            </h4>
                                        </div>
                                        <section class="publicationWrapper">
                                            <div class="content-list">

                                                <div class="table table-responsive">

                                                    <table class="table redtbl table-bordered table-hover dataTable" id="recruiting">
                                                        <thead>
                                                            <th>ID</th>
                                                            <th>LicenseNumber</th>
                                                            <th>RecruitingAgency Name</th>
                                                            <th>Address</th>
                                                            <th>Contact</th>
                                                            <th>Email</th>
                                                            <th>Website</th>
                                                            <th>Status</th>
                                                            <th>Remarks</th>
                                                            <th>Latitude</th>
                                                            <th>Longitude</th>
                                                        </thead>
                                                        <tbody>

                                                        </tbody>
                                                    </table>
                                                    <h2 style="text-align: center";>  NO DATA AVAILABLE.....</h2>
                                                </div>
                                            </div>
                                        </section>

                                    </div>
                                    <!-- this is going to be -->

                                    <div role="tabpanel" class="tab-pane active" id="rules">
                                        <div class="custom-accordion">
                                            <div class="card-header">
                                                <h4>Orientation Centres </h4>
                                            </div>
                                            <section class="publicationWrapper">
                                                <div class="content-list">

                                                <div class="table table-responsive">

                                                    <table class="table redtbl table-bordered table-hover dataTable" id="orientation">
                                                        <thead>
                                                            <tr>
                                                        </thead>
                                                        <tbody>

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            </section>

                                        </div>
                                    </div>



                                    <!-- bulletin -->

                                    <div role="tabpanel" class="tab-pane " id="bulletin">
                                        <div class="custom-accordion">
                                            <div class="card-header">
                                                <h4>Insurance Centres</h4>
                                            </div>
                                            <section class="publicationWrapper">
                                                 <div class="content-list">

                                                <div class="table table-responsive">

                                                    <table class="table redtbl table-bordered table-hover dataTable" id="insurance">
                                                        <thead>
                                                            
                                                        </thead>
                                                        <tbody>

                                                        </tbody>
                                                    </table>

                                                </div>
                                            </div>


                                                
                                            </section>

                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane " id="downloads">
                                        <div class="card-header">
                                            <h4>Migrant Resource Centres
                                            </h4>
                                        </div>
                                        <section class="publicationWrapper">
                                                 <div class="content-list">

                                                <div class="table table-responsive">

                                                    <table class="table redtbl table-bordered table-hover dataTable" id="migrant">
                                                        <thead>
                                                            <th>ID</th>
                                                            <th>MigrantResource Name</th>
                                                            <th>Address</th>
                                                            <th>Contact</th>
                                                            <th>Email</th>
                                                            <th>Website</th>
                                                            <th>Status</th>
                                                            <th>Remarks</th>
                                                            <th>Latitude</th>
                                                            <th>Longitude</th>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                   <h2 style="text-align: center";>  NO DATA AVAILABLE.....</h2>
                                                </div>
                                            </div>


                                                
                                            </section>
                                        
                                    </div>
                                    <div role="tabpanel" class="tab-pane " id="regulation">
                                        <div class="card-header">
                                            <h4>Medical Centres
                                            </h4>
                                        </div>
                                        <section class="publicationWrapper">
                                                 <div class="content-list">

                                                <div class="table table-responsive">

                                                    <table class="table redtbl table-bordered table-hover dataTable" id="medical">
                                                        <thead>
                                                            
                                                        </thead>
                                                        <tbody>

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>


                                                
                                            </section>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <!-- jquery library section  -->

    <script src="{{ asset('frontend/newjs/final/dataTables.min.js') }}"></script>
    <script src="{{ asset('frontend/newjs/final/dataTables.bootstrap.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            var APP_URL = {!! json_encode(url('/')) !!}
            var orientationapi = null;
            $.ajax({
                url: APP_URL + '/orientation', //static_url+'assets/data/Hydropower_Sites.geojson',
                //data: {},
                async: false, //blocks window close
                success: function(data) {
                    orientationapi = JSON.parse(data);

                }
            });
            console.log(orientationapi);
            console.log(orientationapi.features[0].properties);


            $('#orientation').DataTable({
                
                // "pageLength": 50,
                 // iDisplayLength: 50,
                columns: [
                { title: "ID"},
                { title: "PermissionNumber"},
                { title: "Orientation Name"},
                { title: "Address"},
                { title: "Contact"},
                { title: "Email"},
                { title: "Website"},
                { title: "Status"},
                { title: "Remarks"},
                { title: "Latitude"},
                { title: "Longitude"}
            ],
            });
            for(var i=0; i<orientationapi.features.length; i++){
              $('#orientation').DataTable().row.add( [
          orientationapi.features[i].properties.ID ,
            orientationapi.features[i].properties.Permission, 
               orientationapi.features[i].properties.OrName ,
               orientationapi.features[i].properties.Address ,
               orientationapi.features[i].properties.Contact ,
               orientationapi.features[i].properties.Email ,
               orientationapi.features[i].properties.Website ,
               orientationapi.features[i].properties.Status ,
               orientationapi.features[i].properties.Remark,
               orientationapi.features[i].geometry.coordinates[1],
               orientationapi.features[i].geometry.coordinates[0]
        ]).draw( true );
        }

        var insuranceapi = null;
            $.ajax({
                url: APP_URL + '/insurance', //static_url+'assets/data/Hydropower_Sites.geojson',
                //data: {},
                async: false, //blocks window close
                success: function(data) {
                    insuranceapi = JSON.parse(data);

                }
            });
            console.log(insuranceapi);
            // console.log(orientationapi.features[0].properties);


            $('#insurance').DataTable({
                
                // "pageLength": 50,
                 // iDisplayLength: 50,
                columns: [
                { title: "ID"},
                { title: "Insurance Name"},
                { title: "Address"},
                { title: "Contact"},
                { title: "Email"},
                { title: "Website"},
                { title: "Status"},
                { title: "Remarks"},
                { title: "Latitude"},
                { title: "Longitude"}
            ],
            });
            for(var j=1; j<insuranceapi.features.length; j++){
              $('#insurance').DataTable().row.add( [
          insuranceapi.features[j].properties.ID ,
               insuranceapi.features[j].properties.CompanyName ,
               insuranceapi.features[j].properties.Address ,
               insuranceapi.features[j].properties.Contact ,
               insuranceapi.features[j].properties.Email ,
               insuranceapi.features[j].properties.Website ,
               insuranceapi.features[j].properties.Status ,
               insuranceapi.features[j].properties.Remark,
               insuranceapi.features[j].geometry.coordinates[1],
               insuranceapi.features[j].geometry.coordinates[0]
        ]).draw( true );
        }
       






        });

    </script>
@endpush