@extends('layouts.frontmaster')
@push('css')
<link href="{{asset('assets/front/css/aboutus.css')}}" rel="stylesheet">
@endpush
@push('js')
<script>
    $(window).on('load', function() {
        var url = window.location.href;

        var words = url.split('#');
        var check=words[1];
        {{--  alert(check);  --}}
        if(check=='about')
        {

            $('#about').addClass('in active');
            $('#about1').addClass('in active');
            {{--  $('.checkactive4').removeClass('in active');  --}}
            {{--  $("#hownav").css("display", "inline");  --}}



        }
        if(check=='impwork')
        {
            $('#impwork').addClass('in active');
            $('#impwork1').addClass('in active');
            {{--  $("#whatnav").css("display", "inline");  --}}

        }
        if(check=='workofwoda')
        {
            $('#workofwoda').addClass('in active');
            $('#workofwoda1').addClass('in active');
            {{--  $("#missionnav").css("display", "inline");  --}}

        }
        if(check=='sachiwalay')
        {
            $('#sachiwalay').addClass('in active');
            $('#sachiwalay1').addClass('in active');
            {{--  $("#teamnav").css("display", "inline");  --}}

        }
        if(check=='board-team')
        {
            $('#board-team').addClass('in active');
            $('#board-team1').addClass('in active');
            {{--  $("#teamnav").css("display", "inline");  --}}

        }

        })
</script>

@endpush
@section('content')
<div class="pb-breadcrumb" style="background:url({{asset('assets/front/images/bg/1.jpg')}});">
    <div class="breadcrumb-wrap">
        {{--  <h2>Follow steps</h2>  --}}
        <ul>
                <li><a href="{{route('front.index')}}">गृहपृष्ठ</a></li>
                <?php $segments = ''; ?>  @foreach(Request::segments() as $segment) <?php $segments .= '>'.$segment; ?>        <li>
                        <a href="{{ $segments }}">{{$segment}}</a>
                    </li>
                @endforeach
        </ul>
    </div>
</div>
<div class="inner-page section">
    <div class="container">
        <div class="row">
                @if(Session::has('success'))
                            <div class="alert alert-success">
                                {{ Session::get('success') }}
                            </div>
                        @endif
                        @if(Session::has('error'))
                            <div class="alert alert-danger">
                                {{ Session::get('error') }}
                            </div>
                        @endif
           
     

            </div>
        </div>
    </div>

@endsection
