@extends('layouts.frontmaster')

@section('content')
<div class="pb-breadcrumb" style="background:url({{asset('assets/front/images/bg/1.jpg')}});">
    <div class="breadcrumb-wrap">
        {{--  <h2>Follow steps</h2>  --}}

        <ul>
                <li><a href="{{route('front.index')}}">गृहपृष्ठ</a></li>
                <?php $segments = ''; ?>  @foreach(Request::segments() as $segment) <?php $segments .= '/'.$segment; ?>        <li>
                        <a href="{{ $segments }}">{{$segment}}</a>
                    </li>
                @endforeach
        </ul>
    </div>
</div>

<div class="inner-page section">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-lg-3">
                    <div class="side-menu profile-menu">
                        <div class="fepb-profile">
                            <div class="pf-thumb">
                                <img src="{{asset('/member_upload/'.$member->image)}}" alt="profile">
                            </div>
                            <div class="content">
                                <h4><a href="#">{{$member->name}}</a></h4>
                                <span>{{$member->position}}</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-9 col-lg-9">
                    <div class="menu-content profile-content">
                        <div class="card">
                            <div class="card-body">
                                <div class="card-header">
                                    <h4>प्राेफाइल
                                    </h4>
                                </div>
                                <div class="content-list">
                                    <p class="intro">
                                       {!!$member->description!!}
                                    </p>
                                    <div class="table table-responsive">
                                        <table class="table table-bordered">
                                            <tbody>
                                                <tr>
                                                    <td><strong>नाम</strong></td>
                                                    <td><span>{{$member->name}}</span></td>
                                                </tr>
                                                <tr>
                                                    <td><strong>पद</strong></td>
                                                    <td><span>{{$member->position}}</span></td>
                                                </tr>
                                                <tr>
                                                    <td><strong>ठेगाना</strong></td>
                                                    <td><span>{{$member->address}}</span></td>
                                                </tr>
                                                <tr>
                                                    <td><strong>सम्पर्क</strong></td>
                                                    <td><span>{{$member->phone}}</span></td>
                                                </tr>
                                                <tr>
                                                    <td><strong>नियुक्ति मिति</strong></td>
                                                    <td><span>{{$member->joined_from}}</span></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>



@endsection