@extends('layouts.frontmaster')

@push('css')

<link rel="stylesheet" type="text/css" href="{{asset('assets/front/css/magnific-popup.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('assets/front/css/infographics.css')}}">
<link rel="stylesheet" href="{{asset('assets/front/css/multimedia.css')}}">
@endpush
@push('js')
<script src="{{asset('assets/front/js/jquery.magnific-popup.min.js')}}"></script>

@endpush
@section('content')
<div class="pb-breadcrumb" style="background:url({{asset('assets/front/images/bg/1.jpg')}});">
    <div class="breadcrumb-wrap">
        {{--  <h2>Follow steps</h2>  --}}

        <ul>
                <li><a href="{{route('front.index')}}">गृहपृष्ठ</a></li>
                <?php $segments = ''; ?>  @foreach(Request::segments() as $segment) <?php $segments .= '/'.$segment; ?>        <li>
                        <a href="{{ $segments }}">{{$segment}}</a>
                    </li>
                @endforeach
        </ul>
    </div>
</div>

<div class="inner-page section">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <div class="menu-content">
                        <div class="card">
                            <div class="card-body">

                                <div class="tab-content">
                                    <div class="card-header">
                                        <h4>तस्वीर संग्रह</h4>
                                    </div>
                                    <div class="container infographicsWrapper">
                                        <div class="row">
                                            <div class="row">
                                                   
                                                    @foreach ($photos as $item)
                                                   

                                                <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                                                    <a class="thumbnail major-preview" href="{{asset('/blog_upload/'.$item->image)}}">
                                                        <img class="img-thumbnail" src="{{asset('/blog_upload/'.$item->image)}}" alt="Another alt text">
                                                    </a>
                                                </div>
                                                @endforeach

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
