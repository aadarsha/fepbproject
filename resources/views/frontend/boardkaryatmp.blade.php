@extends('layouts.frontmaster')
@push('css')
<style>
    .canvasjs-chart-canvas {

        position: inherit !important;
        width: 550px !important;
        height: 350px !important;

        -webkit-tap-highlight-color: transparent;
        cursor: default;
    }
</style>


@endpush
@push('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
<script src="{{asset('assets/front/js/jquery.magnific-popup.min.js')}}"></script>

<script>
    var count=document.getElementById('count').value;
    var myChart;
    var labeltmp=new Array();
    var valuetmp=new Array();
    var datetmp=new Array();
    var datetmp=new Array();
    var label=[];
    var value=[];
    var tmpcount=0;
    var checkdate=0;
    var category=0;
     
     chart = <?php echo json_encode($chart); ?>;
     date = <?php echo json_encode($date); ?>;
     category_id = <?php echo json_encode($category_id); ?>;    
  
     console.log(date);  
     console.log(category_id);  
     console.log(chart);  

     function filterbydate(sel)
     {
        label=[];
        value=[];
       
         checkdate=sel.value;
            category=document.getElementById('pickdate').getAttribute('data-sth');
            {{--  alert(category);  --}}
         for(i=0;i<count;i++)
         {   
             for(j=0;j<chart.length;j++)
          {
             
             if(category==chart[j]['category_id']&&chart[j]['date']==checkdate) 
             {      
                 label.push(chart[j]['fiscal']);
                 value.push(chart[j]['total']);
             }
          
          }          

           
         var tmpname='tmpchart'+i;
         var tmpname1='#tmpchart'+i;
         
         $(tmpname1).remove();
          
          var app='<div class="content-list" ><canvas id="'+tmpname+'"></canvas></div>';
        
         var chartid='#chart'+i;
         $(chartid).append(app);
         
         var ctx = document.getElementById(tmpname).getContext('2d');
       
         Chart.defaults.global.legend.display = false;

        var  myChart = new Chart(ctx, {
             type: 'bar',
             data: {               
                
                  labels: label,
                  
                  datasets: [{
                     label: '',
                      data: value,                   
                     backgroundColor: [
                         'purple',
                         'red',
                         'yellow',
                         'green',
                         'orange',
                         'blue',
                         'maroon'
                     ],
               
                     borderWidth: 1
                 }]
             },
             options: {
                 scales: {
                     yAxes: [{
                         ticks: {
                             beginAtZero: true
                         }
                     }],
                     xAxes: [{
                         gridLines: {
                             display: false
                         }
                     }],
                 }
             }
         });

 
 
     }
     $('#chartcontainer').remove();
     label=[];
     value=[];
 
     }
     
    window.onload = function () {
       
        label=[];
        value=[];
        for(i=0;i<count;i++)
        {   
            for(j=0;j<chart.length;j++)
         {
            
            if(category_id[i]==chart[j]['category_id']&&chart[j]['date']==date[0]) 
            {      
                label.push(chart[j]['fiscal']);
                value.push(chart[j]['total']);
            }
         
         }  
          
        var tmpname='tmpchart'+i;
        var ctx = document.getElementById(tmpname).getContext('2d');
        Chart.defaults.global.legend.display = false;
       var  myChart = new Chart(ctx, {
            type: 'bar',
            data: {               
               
                 labels: label,
                 
                 datasets: [{
                    label: '',
                     data: value,                   
                    backgroundColor: [
                        'purple',
                        'red',
                        'yellow',
                        'green',
                        'orange',
                        'blue',
                        'maroon'
                    ],
              
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }],
                    xAxes: [{
                        gridLines: {
                            display: false
                        }
                    }],
                }
            }
        });

        label=[];
        value=[];
    }
    }

</script>

@endpush
@section('content')
<div class="pb-breadcrumb" style="background:url({{asset('assets/front/images/bg/1.jpg')}});">
    <div class="breadcrumb-wrap">
        {{--  <h2>Follow steps</h2>  --}}

        <ul>
                <li><a href="{{route('front.index')}}">गृहपृष्ठ</a></li>
                <?php $segments = ''; ?>  @foreach(Request::segments() as $segment) <?php $segments .= '/'.$segment; ?>        <li>
                        <a href="{{ $segments }}">{{$segment}}</a>
                    </li>
                @endforeach
        </ul>
    </div>
</div>
<div class="inner-page section">

    <div class="container">
        <div class="ecotab">
            <div class="economy-nav">
                <ul>
                    <li class="economy-link current" data-tab="ecohelp">आर्थिक सहायता</li>
                    <li class="economy-link" data-tab="otherhelp">सीपमूलक तालिम</li>
                </ul>
            </div>
            <div class="tab-pane">
                <div class="eco-content current" id="ecohelp">
                    <div class="row">
                        <div class="col-md-4 col-lg-4">
                            <div class="side-menu">
                                <ul class="nav nav-tabs" role="tablist">

                                    @foreach ($category_id as $k => $item)                                     

                                                            <li class="nav-item">
                                                                @if ($k==0)
                                                            <a class="nav-link active" href="#chart{{$k}}" role="tab" data-toggle="tab">{{App\ProductCategory::find($item)->name}}</a>
                                                                        @else
                                                                        <a class="nav-link" href="#chart{{$k}}" role="tab" data-toggle="tab">{{App\ProductCategory::find($item)->name}}</a>

                                                                @endif

                                                            </li>
                                                           

                                                          

                                            @endforeach

                                    {{--  <li class="nav-item">
                                         <a class="nav-link" href="#worker" role="tab" data-toggle="tab">मृतक कामदारका परिवारलाई
                                            आर्थिक सहायता </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#handicapped" role="tab" data-toggle="tab">अङ्गभङ्गलाई आर्थिक सहायता</a>
                                    </li>  --}}

                                </ul>
                            </div>
                        </div>
                        <div class="col-md-8 col-lg-8">
                            <div class="menu-content">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="tab-content">
                                         <input type="hidden" id="count" value="{{count($category_id)}}">
                                        
                                    @foreach ($category_id as $k => $item)

                                                         
                                          
                                        @if ($k==0)
                                        <div role="tabpanel" class="tab-pane  in active" id="chart{{$k}}">
                                                @else
                                                <div role="tabpanel" class="tab-pane" id="chart{{$k}}">
                                        @endif
                                      
                                        <div class="card-header">
                                                <h4>{{App\ProductCategory::find($item)->name}}</h4>
                                                <select class="custom-select bar-select" name="pickdate" id="pickdate" data-sth="{{$item}} "onchange="filterbydate(this)">
                                                        <option value="">Select</option> 
                                                    @foreach ($date as $m=>$it) 
                                                                                                     
                                                    <option value="{{$it}}" >{{$it}}</option>                                                        
                                                    @endforeach
                                                  
                                                </select>
                                            </div>
                                            <div class="content-list" id="chartcontainer">
                                            <canvas id="tmpchart{{$k}}"></canvas>
                                            </div>
                                        </div>
                         

                    @endforeach


                                           

                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="eco-content " id="otherhelp">
                    <div class="row">
                        <div class="col-md-4 col-lg-4">
                            <div class="side-menu">
                                <ul class="nav nav-tabs" role="tablist">
                                    @foreach ($trainings as $i=>$item)
                                    <li class="nav-item">
                                            @if ($i==0)
                                    <a class="nav-link active" href="#training{{$i}}" role="tab" data-toggle="tab">{{$item->product_category->name}}</a>
                                                    @else
                                                    <a class="nav-link" href="#training{{$i}}" role="tab" data-toggle="tab">{{$item->product_category->name}}</a>

                                            @endif

                                        </li>
                                    @endforeach

                                </ul>
                            </div>
                        </div>
                        <div class="col-md-8 col-lg-8">
                            <div class="menu-content">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="tab-content">
                                            @foreach ($trainings as $i=>$item)
                                            @if($i==0)
                                                <div role="tabpanel" class="tab-pane in active" id="training{{$i}}">
                                                  @else
                                                  <div role="tabpanel" class="tab-pane" id="training{{$i}}">

                                                    @endif
                                                        <div class="card-header">
                                                            <h4>{{$item->title}}</h4>
                                                        </div>
                                                        <div class="content-list">
                                                            <ul>
                                                             {!!$item->description!!}
                                                            </ul>

                                                        </div>
                                                    </div>


                                            @endforeach


                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
