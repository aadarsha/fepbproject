@extends('adminlte::page')

{{--  @section('title', 'Dashboard')  --}}
@section('content_header')
@yield('content_header')

@stop

@section('content')
<div class="text-center">
    <h2>
        </h2>
    <p> {{Auth::user()->name}} लग इन सफल !</p>
</div>

    @yield('content')
@stop

@section('css')
 {{--dropify--}}
 <link rel="stylesheet" href="{{ asset('assets/dropify/css/dropify.css' )}}">

 {{--dropify--}}
 {{--  validation  --}}
 <style type="text/css">
    .error
    {
        color:red;
    }
</style>
 {{--  validation  --}}
    @yield('css')
@stop

@section('js')

{{--dropify--}}
<script src="{{ asset('assets/dropify/js/dropify.js' )}}"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.dropify').dropify();
    });
</script>
{{--dropify--}}
{{--  ckeditor  --}}
<script type="text/javascript" src="{{asset('assets/ckeditor/ckeditor.js')}}"></script>
{{--  <script src="//cdn.ckeditor.com/4.11.1/standard/ckeditor.js"></script>  --}}
{{--  <script src="//cdn.ckeditor.com/4.11.1/full/ckeditor.js"></script>  --}}
{{--  ckeditor  --}}
{{--  Jqueryvalidation  --}}
<script type="text/javascript" src="{{asset('assets/jqueryvalidation/dist/jquery.validate.min.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#valid_form').validate();
    });
</script>
{{--  Jqueryvalidation  --}}
{{--  select2  --}}
<script>
    $(".myselect").select2(
        {
            placeholder: "Select"

        });
</script>
{{--  select2  --}}

{{--  datatable  --}}
<script type="text/javascript">
    $(document).ready(function(){
        $('#data_table').DataTable();
    });
</script>
{{--  datatable  --}}

{{--  multifield  --}}


    @stack('js')
    @yield('js')
@stop

