@extends('layouts.backmaster')

@section('content')
    <div class="container">
        <div class="row">
                <div class="col-md-8 col-lg-8 col-xl-8 col-sm-8 col-xs-8 col-lg-offset-2 col-md-offset-2 col-xs-offset-2 col-sm-offset-2 col-xl-offset-2">
                        <div class="panel panel-default">
                    <div class="panel-heading">
                        <label>Create </label>
                        <a href="{{route('general.index')}}" class="btn btn-primary pull-right"> List</a>
                    </div>

                    <div class="panel-body">
                            @if(!isset($general->id))
                            <form method="post" action="{{route('general.store')}}"  enctype="multipart/form-data" id="valid_form">
                                <input type ="hidden" class="form-control" name ="created_by" value="{{Auth::user()->id}}">

                                @else
                                <form method="post" action="{{route('general.update',$general)}}" method="post" enctype="multipart/form-data" id="valid_form">
                                        <input type="hidden" name="_method" value="put">

                                        <input type ="hidden" class="form-control" name ="updated_by" value="{{Auth::user()->id}}">
                                        <input type ="hidden" class="form-control" name ="created_by" value="{{$general->created_by}}">
                             @endif
                            {{csrf_field()}}
                            <div class="form-group">
                                    <label for="title">Title</label>
                                    <input type ="text" class="form-control" name ="title" id="title" value="{{$general->title}}" required>
                                    @if ($errors->has('title'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('title') }}</strong>
                                        </span>
                                    @endif

                                </div>



                           <div class="form-group">
                                <span class="data-label" for="category">general Type</span>
                                <select class="form-control myselect" name="category" id="category" required>
                                        <option selected value="{{$general->category}}">{{$general->category}}</option>
                                        @foreach($categories as $category)
                                        <option value="{{$category->name}}">{{$category->name}}</option>
                                    @endforeach
                                </select>

                        </div>
                            <div class="form-group">
                                <label>Status</label>
                                <input type ="radio"  name ="status" checked value="1">Active
                                <input type ="radio"  name ="status" value="0" > Deactive
                            </div>
                            <div class="form-group">
                                <label for="note">Note</label>
                                <textarea class="form-control ckeditor" name ="note" id="note"  required>{!!$general->note!!} </textarea>
                                @if ($errors->has('note'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('note') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="short_description">Short Description</label>
                                <textarea class="form-control ckeditor" name ="short_description" id="short_description"  required>{!!$general->short_description!!} </textarea>
                                @if ($errors->has('short_description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('short_description') }}</strong>
                                    </span>
                                @endif

                            </div>



                            <div class="form-group">
                                <input class="btn btn-success" type ="submit" name ="submit" value="Save general">

                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
