@extends('layouts.backmaster')


@section('content')
    <div class="container">
        <div class="row">
                <div class="col-md-8 col-lg-8 col-xl-8 col-sm-8 col-xs-8 col-lg-offset-2 col-md-offset-2 col-xs-offset-2 col-sm-offset-2 col-xl-offset-2">
                        <div class="panel panel-default">
                    <div class="panel-heading">
                        <label> List</label>
                        <a href="{{route('multimediavideo.create')}}" class="btn btn-primary pull-right">Add भिडियो, चेतना</a>
                        <a href="{{route('multimediaaudio.create')}}" class="btn btn-primary pull-right">Add अडियो, चेतना</a>
                        <a href="{{route('multimediaphoto.create')}}" class="btn btn-primary pull-right">Add तस्विर, चेतना</a>
                        <a href="{{route('faq.create')}}" class="btn btn-primary pull-right">Add बारम्बार सोधिने प्रश्न</a>
                        <a href="{{route('institute.create')}}" class="btn btn-primary pull-right">Add सहयोगी संस्थाहरु</a>
                        <a href="{{route('basicnote.create')}}" class="btn btn-primary pull-right">Add कामदारहरुले ध्यान दिनु पर्ने कुराहरु</a>
                           </div>
                           {{-- commented here --}}

                    <div class="panel-body">
                        @if(Session::has('success'))
                            <div class="alert alert-success">
                                {{ Session::get('success') }}
                            </div>
                        @endif
                        @if(Session::has('error'))
                            <div class="alert alert-danger">
                                {{ Session::get('error') }}
                            </div>
                        @endif

                        <table class="table table-bordered" id="data_table">
                            <thead>
                            <tr>

                                <th>SN</th>
                                <th>Title</th>
                                <th>Category</th>
                                <th>Status</th>
                                <th>Action</th>


                            </tr>
                            </thead>

                            <tbody>
                            @php($i=1)
                            @foreach($multimedia as $s)
                                <tr>

                                    <td>{{$i++}}</td>
                                    <td>{{$s->title}}</td>
                                    <td>{{$s->category}}</td>

                                    <td>
                                        @if($s->status==1)
                                            <label class="label label-success">Active</label>
                                        @else
                                            <label class="label label-danger">Inactive</label>
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{route('multimedia.edit',$s)}}" class="btn btn-warning btn-block">Edit</a>
                                        <a href="{{route('multimedia.show',$s)}}" class="btn btn-info btn-block">View Detail</a>

                                        <form action="{{route('multimedia.destroy',$s)}}" method="post" onsubmit="return confirm('Are You Sure??')">
                                            <input type="hidden" name="_method" value="delete">
                                            {{csrf_field()}}
                                            <input type="submit" value="Delete" class="btn btn-danger btn-block" >
                                        </form>
                                    </td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

