
@extends('layouts.backmaster')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <label>member Detail</label>
                        <a href="{{route('member.index')}}" class="btn btn-primary pull-right">member List</a>
                    </div>

                    <div class="panel-body">
                        <table class="table table-bordered">
                            <tr>
                                <th>Name</th>
                                <td>{{$member->name}}</td>
                            </tr>
                            <tr>
                                <th>Image</th>
                                <td><img src="{{asset('/member_upload/'.$member->image)}}" height="100" width="100"></td>


                            </tr>
                            <tr>
                                <th>Position</th>
                                <td>{{$member->position}}</td>
                            </tr>
                            <tr>
                                <th>description</th>
                                <td>{!! $member->description !!}}</td>
                            </tr>
                            <tr>
                                <th>Map Location</th>
                                <td>{!! $member->map !!}}</td>
                            </tr>
                            <tr>
                                <th>Education</th>
                                <td>{{$member->education}}</td>
                            </tr>
                            <tr>
                                <th>Address</th>
                                <td>{{$member->address}}</td>
                            </tr>
                            <tr>
                                <th>Email</th>
                                <td>{{$member->email}}</td>
                            </tr>
                            <tr>
                                <th>Website</th>
                                <td>{{$member->website}}</td>
                            </tr>
                            <tr>
                                <th>Rank</th>
                                <td>{{$member->rank}}</td>
                            </tr>


                            <tr>
                                <th>Status</th>
                                <td>
                                    @if($member->status==1)
                                        <label class="label label-success">Active </label>
                                    @else
                                        <label class="label label-danger">Inactive</label>
                                    @endif
                                </td>


                            </tr>


                            <tr>
                                <th>Created By</th>
                                <td>{{\App\User::find($member->created_by)->name}}</td>
                            </tr>
                            <tr>
                                <th>Updated By</th>
                                <td>
                                    @if(!empty($member->updated_by))
                                        {{\App\User::find($member->updated_by)->name}}
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th>Created At</th>
                                <td>{{$member->created_at}}</td>
                            </tr>
                            <tr>
                                <th>Updated At</th>
                                <td>{{$member->updated_at}}</td>
                            </tr>

                            <tr>
                                <td>
                                    <a href="{{route('member.edit',$member->id)}}" class="btn btn-warning">Edit member</a>
                                </td>
                                <td>
                                    <form action="{{route('member.destroy',$member->id)}}" method="post" onsubmit="return confirm('Are You Sure ???')">
                                        <input type="hidden" name="_method" value="delete">
                                        {{csrf_field()}}
                                        <input type="submit" value="Delete" class="btn btn-danger">
                                    </form>

                                </td>
                            </tr>

                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
