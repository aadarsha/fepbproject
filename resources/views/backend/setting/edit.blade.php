@extends('layouts.backmaster')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <label>Edit सेटिंग</label>
                        <a href="{{route('setting.index')}}" class="btn btn-primary pull-right">सेटिंग List </a>
                    </div>

                    <div class="panel-body">
                        <form  action="{{route('setting.update',$setting)}}" enctype="multipart/form-data" method="post">
                            <input type="hidden" name="_method" value="put">
                            {{csrf_field()}}
                            {{--  <div class="form-group">
                                <label for="logo">Logo</label>
                                <input type ="text" class="form-control" name ="logo" value="{{$setting->logo}}" id="logo" required>
                                @if ($errors->has('logo'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('logo') }}</strong>
                                    </span>
                                @endif

                            </div>  --}}
                            <div class="form-group">
                                <label for="logo">logo</label>
                                {{--  <input type="text" name="logo" id="logo" class="form-control" value="{{$setting->logo}}">  --}}
                                <input type="file" name="logo" id="logo" class="form-control">
                                <td><img src="{{asset('/setting_upload/'.$setting->logo)}}" height="100" width="100"></td>
                                {{--  <input type="file" name="image" id="image" class="form-control" >  --}}

                            </div>

                            <div class="form-group">
                                <label for="name">Name</label>

                                <input type ="text" class="form-control" name ="name" id="name" value="{{$setting->name}}" required >
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif

                            </div>



                            <div class="form-group">
                                <label for="address">Address</label>
                                <input type ="text" class="form-control" name ="address" id="address"  value="{{$setting->address}}"   required>
                                @if ($errors->has('address'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                @endif

                            </div>

                            <div class="form-group">
                                <label for="phone1">Phone</label>
                                <input type ="text" class="form-control" name ="phone1" id="phone1"  value="{{$setting->phone1}}"   required>
                                @if ($errors->has('phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif

                            </div>
                            <div class="form-group">
                                <label for="phone2">Phone</label>
                                <input type ="text" class="form-control" name ="phone2" id="phone2"  value="{{$setting->phone2}}"   required>
                                @if ($errors->has('phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif

                            </div>
                            <div class="form-group">
                                <label for="fax">Fax</label>
                                <input type ="text" class="form-control" name ="fax" id="fax" value="{{$setting->fax}}"  required>
                                @if ($errors->has('fax'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('fax') }}</strong>
                                    </span>
                                @endif

                            </div>



                            <div class="form-group">
                                <label for="email"> Email</label>
                                <input type ="email" class="form-control" name ="email" id="email" value="{{$setting->email}}" required>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif

                            </div>

                            <div class="form-group">
                                <label for="facebook_link">Facebook Link</label>
                                <input type ="text" class="form-control" name ="facebook_link" id="facebook_link" value="{{$setting->facebook_link}}">
                                @if ($errors->has('facebook_link'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('facebook_link') }}</strong>
                                    </span>
                                @endif

                            </div>
                            <div class="form-group">
                                <label for="twitter_link">Twitter Link</label>
                                <input type ="text" class="form-control" name ="twitter_link" id="twitter_link" value="{{$setting->twitter_link}}">
                                @if ($errors->has('twitter_link'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('twitter_link') }}</strong>
                                    </span>
                                @endif

                            </div>
                            <div class="form-group">
                                <label for="linkedin_link">Linkedin Link</label>
                                <input type ="text" class="form-control" name ="linkedin_link" id="linkedin_link" value="{{$setting->linkedin_link}}">
                                @if ($errors->has('linkedin_link'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('linkedin_link') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="website">Website Link</label>
                                <input type ="text" class="form-control" name ="website" id="website" value="{{$setting->website}}">
                                @if ($errors->has('website'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('website') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group">
                                    <label for="tollfree">Toll Free Number</label>
                                    <input type ="text" class="form-control" name ="tollfree" id="tollfree" value="{{$setting->tollfree}}">
                                    @if ($errors->has('tollfree'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('tollfree') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                        <label for="youtube_link">Youtube Link</label>
                                        <input type ="text" class="form-control" name ="youtube_link" id="youtube_link" value="{{$setting->youtube_link}}">
                                        @if ($errors->has('youtube_link'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('youtube_link') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                            <div class="form-group">
                                <!-- <label>Created By</label> -->
                                <input type ="hidden" class="form-control" name ="created_by" value="{{Auth::user()->id}}">
                            </div>


                            <div class="form-group">
                                <input class="btn btn-success" type ="submit" name ="submit" value="Save Setting">

                            </div>


                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

