@extends('adminlte::page')

@section('content')
    <div class="container">
        <div class="row">
                <div class="col-md-8 col-lg-8 col-xl-8 col-sm-8 col-xs-8 col-lg-offset-2 col-md-offset-2 col-xs-offset-2 col-sm-offset-2 col-xl-offset-2">
                        <div class="panel panel-default">
                    <div class="panel-heading">
                        <label>Setting Detail</label>
                        <a href="{{route('setting.index')}}" class="btn btn-primary pull-right">Setting सबै इन्ट्रीहरु </a>
                    </div>

                    <div class="panel-body">
                        <table class="table table-bordered ">
                            <tr>
                                <th>Logo</th>
                                <td><img src="{{asset('/setting_upload/'.$setting->logo)}}" height="100" width="100"></td>
                            </tr>
                            <tr>
                                <th>Name</th>
                                <td>{{$setting->name}}</td>
                            </tr>

                            <tr>
                                <th>Address</th>
                                <td>{{$setting->address}}</td>
                            </tr>

                            <tr>
                                <th>Phone</th>
                                <td>{{$setting->phone}}</td>
                            </tr>

                            <tr>
                                <th> Email</th>
                                <td>{{$setting->email}}</td>
                            </tr>

                            <tr>
                                <th>Facebook Link</th>
                                <td>{{$setting->facebook_link}}</td>
                            </tr>
                            <tr>
                                <th>Twitter Link</th>
                                <td>{{$setting->twitter_link}}</td>
                            </tr>
                            <tr>
                                <th>Linkedin Link</th>
                                <td>{{$setting->linkedin_link}}</td>
                            </tr>
                            <tr>
                                <th>Google Link</th>
                                <td>{{$setting->google_link}}</td>
                            </tr>


                            <tr>
                                <th>Created At</th>
                                <td>{{$setting->created_at}}</td>
                            </tr>
                            <tr>
                                <th>Updated At</th>
                                <td>{{$setting->updated_at}}</td>
                            </tr>

                            <tr>
                                <td>
                                    <a href="{{route('setting.edit',$setting)}}" class="btn btn-warning">Edit Setting</a>

                                </td>
                                <td>
                                    {{--  <form action="{{route('setting.destroy',$setting)}}" method="post">
                                        <input type="hidden" name="_method" value="delete">
                                        {{csrf_field()}}
                                        <input type="submit" value="Delete" class="btn btn-danger">
                                    </form>  --}}

                                </td>
                            </tr>
                        </table>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

