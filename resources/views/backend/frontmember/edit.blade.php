
@extends('layouts.backmaster')


@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <label>Create frontmember</label>
                        <a href="{{route('frontmember.index')}}" class="btn btn-primary pull-right">frontmember List</a>
                    </div>

                    <div class="panel-body col-md-8 col-md-offset-2">

                        <form action="{{route('frontmember.update',$frontmember->id)}}" method="post" enctype="multipart/form-data" id="valid_form">
                            <input type="hidden" name="_method" value="put">
                            {{csrf_field()}}

                            <div class="form-group ">
                                <label for="name">Name</label>
                                <input class="form-control" type="text" name="name" id="name" value="{{$frontmember->name}}" >
                            </div>

                            <div class="form-group">

                                <input type="hidden" name="image" id="image" class="form-control" value="{{$frontmember->image}}" >

                                <label for="image">Change image</label>
                                <input type="file" name="image" id="image" class="form-control dropify" value="{{$frontmember->image}}">
                                <td><img src="{{asset('/frontmember_upload/'.$frontmember->image)}}" height="100" width="100"></td>
                            </div>
                            <div class="form-group ">
                                <label for="position">Position</label>
                                <input class="form-control" type="text" name="position" id="position" value="{{$frontmember->position}}" >
                            </div>
                            <div class="form-group ">
                                <label for="position">Catageory</label>
                                <select class="form-control myselect" name="category" id="category" value="{{$frontmember->category}}">
                                    <option value="normal">YI-Lab Team</option>
                                    <option value="exclusive">YI-Lab Advisor</option>
                                </select>
                            </div>
                            <div class="form-group ">
                                <label for="description">Description</label>
                                <textarea class="form-control ckeditor"  name="description" id="description" >{{$frontmember->description}}</textarea>
                            </div>
                            {{--<div class="form-group ">--}}
                                {{--<label for="description">Map Location</label>--}}
                                {{--<textarea class="form-control ckeditor"  name="map" id="map" >{{$frontmember->map}}</textarea>--}}
                            {{--</div>--}}
                            {{--  <div class="form-group ">
                                <label for="education">Education</label>
                                <input class="form-control" type="text" name="education" id="education" value="{{$frontmember->education}}" >
                            </div>
                            <div class="form-group ">
                                <label for="address">Address</label>
                                <input class="form-control" type="text" name="address" id="address" value="{{$frontmember->address}}" >
                            </div>
                            <div class="form-group ">
                                <label for="phone">Phone</label>
                                <input class="form-control" type="text" name="phone" id="phone" value="{{$frontmember->phone}}" >
                            </div>
                            <div class="form-group ">
                                <label for="email">email</label>
                                <input class="form-control" type="email" name="email" id="email" value="{{$frontmember->email}}" >
                            </div>
                            <div class="form-group ">
                                <label for="website">Website</label>
                                <input class="form-control" type="text" name="website" id="website" value="{{$frontmember->website}}" >
                            </div>  --}}
                            <div class="form-group ">
                                <label for="fb_link">Facebook Link</label>
                                <input class="form-control" type="text" name="fb_link" id="fb_link" value="{{$frontmember->fb_link}}" >
                            </div>
                            <div class="form-group ">
                                <label for="twitter_link">Twitter Link</label>
                                <input class="form-control" type="text" name="twitter_link" id="twitter_link" value="{{$frontmember->twitter_link}}" >
                            </div>
                            <div class="form-group ">
                                <label for="linkedin_link">Linkedin Link</label>
                                <input class="form-control" type="text" name="linkedin_link" id="linkedin_link" value="{{$frontmember->linkedin_link}}" >
                            </div>
                            <div class="form-group ">
                                <label for="insta_link">Instagram Link</label>
                                <input class="form-control" type="text" name="insta_link" id="insta_link" value="{{$frontmember->insta_link}}" >
                            </div>





                            <div class="form-group">
                                <label for="status">Status</label>
                                <input type="radio" name="status" id="status" value="1" checked>Active
                                <input type="radio" name="status" id="status" value="0"  >Inactive

                            </div>

                            <div class="form-group">
                                <input type ="hidden" class="form-control" name ="created_by" value="{{Auth::user()->id}}">

                            </div>
                            <div class="form-group col-md-4 col-md-offset-4">
                                <input class="btn btn-success" type ="submit" name ="submit" value="Update frontmember">

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection


