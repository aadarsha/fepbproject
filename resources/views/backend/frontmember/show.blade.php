
@extends('layouts.backmaster')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <label>frontmember Detail</label>
                        <a href="{{route('frontmember.index')}}" class="btn btn-primary pull-right">frontmember List</a>
                    </div>

                    <div class="panel-body">
                        <table class="table table-bordered">
                            <tr>
                                <th>Name</th>
                                <td>{{$frontmember->name}}</td>
                            </tr>
                            <tr>
                                <th>Image</th>
                                <td><img src="{{asset('/frontmember_upload/'.$frontmember->image)}}" height="100" width="100"></td>


                            </tr>
                            <tr>
                                <th>Position</th>
                                <td>{{$frontmember->position}}</td>
                            </tr>
                            <tr>
                                <th>description</th>
                                <td>{!! $frontmember->description !!}}</td>
                            </tr>
                            <tr>
                                <th>Map Location</th>
                                <td>{!! $frontmember->map !!}}</td>
                            </tr>
                            <tr>
                                <th>Education</th>
                                <td>{{$frontmember->education}}</td>
                            </tr>
                            <tr>
                                <th>Address</th>
                                <td>{{$frontmember->address}}</td>
                            </tr>
                            <tr>
                                <th>Email</th>
                                <td>{{$frontmember->email}}</td>
                            </tr>
                            <tr>
                                <th>Website</th>
                                <td>{{$frontmember->website}}</td>
                            </tr>
                            <tr>
                                <th>Rank</th>
                                <td>{{$frontmember->rank}}</td>
                            </tr>


                            <tr>
                                <th>Status</th>
                                <td>
                                    @if($frontmember->status==1)
                                        <label class="label label-success">Active </label>
                                    @else
                                        <label class="label label-danger">Inactive</label>
                                    @endif
                                </td>


                            </tr>


                            <tr>
                                <th>Created By</th>
                                <td>{{\App\User::find($frontmember->created_by)->name}}</td>
                            </tr>
                            <tr>
                                <th>Updated By</th>
                                <td>
                                    @if(!empty($frontmember->updated_by))
                                        {{\App\User::find($frontmember->updated_by)->name}}
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th>Created At</th>
                                <td>{{$frontmember->created_at}}</td>
                            </tr>
                            <tr>
                                <th>Updated At</th>
                                <td>{{$frontmember->updated_at}}</td>
                            </tr>

                            <tr>
                                <td>
                                    <a href="{{route('frontmember.edit',$frontmember->id)}}" class="btn btn-warning">Edit frontmember</a>
                                </td>
                                <td>
                                    <form action="{{route('frontmember.destroy',$frontmember->id)}}" method="post" onsubmit="return confirm('Are You Sure ???')">
                                        <input type="hidden" name="_method" value="delete">
                                        {{csrf_field()}}
                                        <input type="submit" value="Delete" class="btn btn-danger">
                                    </form>

                                </td>
                            </tr>

                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
