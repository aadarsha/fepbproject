@extends('layouts.backmaster')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <label>frontmember List</label>
                        <a href="{{route('frontmember.create')}}" class="btn btn-primary pull-right">Create frontmember</a>
                    </div>

                    <div class="panel-body col-md-10 col-md-offset-1">
                        @if(Session::has('success'))
                            <div class="alert alert-success">
                                {{ Session::get('success') }}
                            </div>
                        @endif
                        @if(Session::has('error'))
                            <div class="alert alert-danger">
                                {{ Session::get('error') }}
                            </div>
                        @endif

                        <table  class="table table-bordered" id="data_table">

                            <thead>
                            <tr>
                                <th>SN</th>
                                <th>Name</th>
                                <th>Image</th>
                                <th>Position</th>

                                <th>Status</th>
                                <th>Action</th>
                            </tr>

                            </thead>
                            <tbody>
                            @php($i=1)
                                @foreach($frontmember as $g)
                                    <tr>
                                        <td>{{$i++}}</td>
                                        <td>{{$g->name}}</td>
                                        <td>
                                            <img src="{{asset('/frontmember_upload/'.$g->image)}}" height="100" width="100">
                                        </td>
                                        <td>{{$g->position}}</td>



                                        <td>
                                            @if($g->status==1)
                                                <label class="label label-success">Active</label>
                                            @else
                                                <label class="label label-danger">Inactive</label>
                                            @endif
                                        </td>

                                        <td>
                                            <a href="{{route('frontmember.edit',$g->id)}}" class="btn btn-warning btn-block">Edit</a>
                                            <a href="{{route('frontmember.show',$g->id)}}" class="btn btn-info btn-block">View Detail</a>

                                            <form action="{{route('frontmember.destroy',$g->id)}}" method="post" onsubmit="return confirm('Are You Sure??')">
                                                <input type="hidden" name="_method" value="delete">
                                                {{csrf_field()}}
                                                <input type="submit" value="Delete" class="btn btn-danger btn-block" >
                                            </form>
                                        </td>

                                    </tr>

                                @endforeach
                            </tbody>

                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
