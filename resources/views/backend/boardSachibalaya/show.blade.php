@extends('layouts.backmaster')

@section('content')
    <div class="container">
        <div class="row">
                <div class="col-md-8 col-lg-8 col-xl-8 col-sm-8 col-xs-8 col-lg-offset-2 col-md-offset-2 col-xs-offset-2 col-sm-offset-2 col-xl-offset-2">
                        <div class="panel panel-default">
                    <div class="panel-heading">
                        <label>बोर्डको सचिवालय Detail</label>
                        <a href="{{route('boardSachibalaya.index')}}" class="btn btn-primary pull-right">बोर्डको सचिवालय List</a>
                    </div>

                    <div class="panel-body">
                        <table class="table table-bordered ">
                            <tr>
                                <th>Title</th>
                                <td>{{$boardSachibalaya->title}}</td>
                            </tr>




                            <tr>
                                <th>Status</th>
                                <td>
                                    @if($boardSachibalaya->status==1)
                                        <label class="label label-success">Active</label>
                                    @else
                                        <label class="label label-danger">Deactive</label>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th>Description</th>
                                <td>{!!$boardSachibalaya->description!!}</td>
                            </tr>
                            <tr>
                                <th>Post</th>
                                <td>{{$boardSachibalaya->post}}</td>
                            </tr>
                            <tr>
                                <th>Created By</th>
                                <td>{{\App\User::find($boardSachibalaya->created_by)->name}}</td>
                            </tr>
                            <tr>
                                <th>Updated By</th>
                                <td>
                                    @if(!empty($boardSachibalaya->updated_by))
                                        {{\App\User::find($boardSachibalaya->updated_by)->name}}
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th>Created At</th>
                                <td>{{$boardSachibalaya->created_at}}</td>
                            </tr>
                            <tr>
                                <th>Updated At</th>
                                <td>{{$boardSachibalaya->updated_at}}</td>
                            </tr>

                            <tr>
                                <td>
                                    <a href="{{route('boardSachibalaya.edit',$boardSachibalaya)}}" class="btn btn-warning">Edit boardSachibalaya</a>
                                </td>
                                <td>
                                    <form action="{{route('boardSachibalaya.destroy',$boardSachibalaya)}}" method="post" onsubmit="return confirm('Are You Sure??')">
                                        <input type="hidden" name="_method" value="delete">
                                        {{csrf_field()}}
                                        <input type="submit" value="Delete" class="btn btn-danger">
                                    </form>

                                </td>
                            </tr>
                        </table>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

