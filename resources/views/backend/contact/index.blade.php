@extends('layouts.backmaster')

@section('css')
    <link type="text/css" rel="stylesheet" href="{{asset('assets/datatables/css/jquery.dataTables.min.css')}}">
@endsection

@section('js')
    <script type="text/javascript" src="{{asset('assets/datatables/js/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#contact_table').DataTable();
        });
    </script>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <label>Contact List</label>
                        <a href="{{route('contact.create')}}" class="btn btn-primary pull-right">contact List</a>

                    </div>

                    <div class="panel-body">
                        @if(Session::has('success'))
                            <div class="alert alert-success">
                                {{ Session::get('success') }}
                            </div>
                        @endif
                        @if(Session::has('error'))
                            <div class="alert alert-danger">
                                {{ Session::get('error') }}
                            </div>
                        @endif

                        <table  class="table table-bordered" id="data_table">

                            <thead>
                            <th>SN</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Subject</th>
                            <th>Date</th>
                            <th>Action</th>
                            </thead>
                            <tbody>
                            @php($i=1)
                            @foreach($contact as $c)
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td>{{$c->name}}</td>
                                    <td>{{$c->email}}</td>
                                    <td>{{$c->phone}}</td>
                                    <td>{{$c->subject}}</td>
                                    <td>{{$c->message}}</td>
                                    <td>
                                        {{-- <a href="{{route('contact.edit',$c)}}" class="btn btn-warning btn-block">Edit</a> --}}
                                        <a href="{{route('contact.show',$c)}}" class="btn btn-info btn-block">View Detail</a>

                                        <form action="{{route('contact.destroy',$c)}}" method="post" onsubmit="return confirm('Are You Sure??')">
                                            <input type="hidden" name="_method" value="delete">
                                            {{csrf_field()}}
                                            <input type="submit" value="Delete" class="btn btn-danger btn-block" >
                                        </form>
                                    </td>
                                </tr>


                            @endforeach

                            </tbody>

                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
