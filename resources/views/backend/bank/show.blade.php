@extends('layouts.backmaster')

@section('content')
    <div class="container">
        <div class="row">
                <div class="col-md-8 col-lg-8 col-xl-8 col-sm-8 col-xs-8 col-lg-offset-2 col-md-offset-2 col-xs-offset-2 col-sm-offset-2 col-xl-offset-2">
                        <div class="panel panel-default">
                    <div class="panel-heading">
                        <label>bank Detail</label>
                        <a href="{{route('bank.index')}}" class="btn btn-primary pull-right">bank List</a>
                    </div>

                    <div class="panel-body">
                        <table class="table table-bordered ">
                            <tr>
                                <th>bank name</th>
                                <td>{{$bank->bank_name}}</td>
                            </tr>




                            <tr>
                                <th>Status</th>
                                <td>
                                    @if($bank->status==1)
                                        <label class="label label-success">Active</label>
                                    @else
                                        <label class="label label-danger">Deactive</label>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th>account</th>
                                <td>{{$bank->account}}</td>
                            </tr>

                            <tr>
                                <th>Created By</th>
                                <td>{{\App\User::find($bank->created_by)->name}}</td>
                            </tr>
                            <tr>
                                <th>Updated By</th>
                                <td>
                                    @if(!empty($bank->updated_by))
                                        {{\App\User::find($bank->updated_by)->name}}
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th>Created At</th>
                                <td>{{$bank->created_at}}</td>
                            </tr>
                            <tr>
                                <th>Updated At</th>
                                <td>{{$bank->updated_at}}</td>
                            </tr>

                            <tr>
                                <td>
                                    <a href="{{route('bank.edit',$bank)}}" class="btn btn-warning">Edit bank</a>
                                </td>
                                <td>
                                    {{--  <form action="{{route('bank.destroy',$bank)}}" method="post">
                                        <input type="hidden" name="_method" value="delete">
                                        {{csrf_field()}}
                                        <input type="submit" value="Delete" class="btn btn-danger">
                                    </form>  --}}

                                </td>
                            </tr>
                        </table>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

