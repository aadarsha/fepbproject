@extends('layouts.backmaster')


@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-lg-8 col-xl-8 col-sm-8 col-xs-8 col-lg-offset-2 col-md-offset-2 col-xs-offset-2 col-sm-offset-2 col-xl-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <label>Create janaGunaso</label>
                        <a href="{{route('janaGunaso.index')}}" class="btn btn-primary pull-right">janaGunaso List</a>
                    </div>

                    <div class="panel-body">
                            @if(!isset($janaGunaso->id))
                            <form method="post" action="{{route('janaGunaso.store')}}"  enctype="multipart/form-data" id="valid_form" >
                                {{ csrf_field() }}

                                @else
                                <form method="post" action="{{route('janaGunaso.update',$janaGunaso)}}" method="post" enctype="multipart/form-data" id="valid_form">
                                        <input type="hidden" name="_method" value="put">
                                        {{csrf_field()}}


                             @endif


                            <div class="form-group">
                                <label for="name">Name</label>

                                <input type ="text" class="form-control" name ="name" id="name" value="{{$janaGunaso->name}}" required >
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif

                            </div>





                            <div class="form-group">
                                <label for="phone1">Phone</label>
                                <input type ="text" class="form-control" name ="phone" id="phone"  value="{{$janaGunaso->phone}}"   required>
                                @if ($errors->has('phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif

                            </div>


                            <div class="form-group">
                                <label for="email"> Email</label>
                                <input type ="email" class="form-control" name ="email" id="email" value="{{$janaGunaso->email}}" required>
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif

                            </div>

                            <div class="form-group">
                                <label for="address">address</label>
                                <input type ="text" class="form-control" name ="address" id="address" value="{{$janaGunaso->address}}">
                                @if ($errors->has('address'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                @endif

                            </div>
                            <div class="form-group">
                                <label for="message">Message</label>
                                <textarea class="form-control ckeditor" name ="message" id="message"  required>{!!$janaGunaso->message!!} </textarea>
                                @if ($errors->has('message'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('message') }}</strong>
                                    </span>
                                @endif

                            </div>
                            <div class="form-group">
                                <label for="message">File</label>
                                <input type="file" name="image" id="image">


                            </div>


                            <div class="form-group">
                                <input class="btn btn-success" type ="submit" name ="submit" value="Save janaGunaso">

                            </div>

                        </form>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
