@extends('adminlte::page')


@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <label>janaGunaso Detail</label>
                        <a href="{{route('janaGunaso.index')}}" class="btn btn-primary pull-right">janaGunaso List</a>
                    </div>

                    <div class="panel-body">
                        <table class="table table-bordered">
                            <tr>
                                <th>Name</th>
                                <td>{{$janaGunaso->name}}</td>
                            </tr>

                            <tr>
                                <th>Email</th>
                                <td>{{$janaGunaso->email}}</td>
                            </tr>

                            <tr>
                                <th>Phone</th>
                                <td>{{$janaGunaso->phone}}</td>
                            </tr>
                            <tr>
                                <th>Subject</th>
                                <td>{{$janaGunaso->subject}}</td>
                            </tr>

                            <tr>
                                <th>Message</th>
                                <td>{{$janaGunaso->message}}</td>
                            </tr>

                            <tr>
                                <th>Date</th>
                                <td>{{$janaGunaso->date}}</td>
                            </tr>

                            <tr>
                                <th>Remarks</th>
                                <td>{{$janaGunaso->remarks}}</td>
                            </tr>

                            <tr>
                                <th>Created At</th>
                                <td>{{$janaGunaso->created_at}}</td>
                            </tr>
                            <tr>
                                <th>Updated At</th>
                                <td>{{$janaGunaso->updated_at}}</td>
                            </tr>
                            <tr>
                                {{--  <td>
                                    <a href="{{route('janaGunaso.edit',$janaGunaso)}}" class="btn btn-warning">Edit janaGunaso</a>
                                </td>  --}}
                                <td>
                                    <form action="{{route('janaGunaso.destroy',$janaGunaso)}}" method="post">
                                        <input type="hidden" name="_method" value="delete">
                                        {{csrf_field()}}
                                        <input type="submit" value="Delete" class="btn btn-danger">
                                    </form>

                                </td>
                            </tr>


                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
