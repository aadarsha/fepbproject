@extends('layouts.backmaster')



@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <label>Create बोर्डको परिचय  </label>
                        <a href="{{route('about.index')}}" class="btn btn-primary pull-right">बोर्डको परिचय List List</a>
                    </div>

                    <div class="panel-body">
                            @if(!isset($about->id))
                            <form method="post" action="{{route('about.store')}}"  enctype="multipart/form-data" id="valid_form">
                                <input type ="hidden" class="form-control" name ="created_by" value="{{Auth::user()->id}}">

                                @else
                                <form method="post" action="{{route('about.update',$about)}}" method="post" enctype="multipart/form-data" id="valid_form">
                                        <input type="hidden" name="_method" value="put">

                                        <input type ="hidden" class="form-control" name ="updated_by" value="{{Auth::user()->id}}">
                                        <input type ="hidden" class="form-control" name ="created_by" value="{{$about->created_by}}">
                             @endif
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="title">Title</label>
                                <input type ="text" class="form-control" name ="title" id="title" value="{{$about->title}}" required>
                                @if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif

                            </div>



                            <div class="form-group">
                                <label>Status</label>
                                <input type ="radio"  name ="status" checked value="1">Active
                                <input type ="radio"  name ="status" value="0" > Deactive
                            </div>
                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea class="form-control ckeditor" name ="description" id="description"  required>{!!$about->description!!} </textarea>
                                @if ($errors->has('description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif

                            </div>
                            <div class="form-group">
                                <label for="short_description">Short Description</label>
                                <textarea class="form-control ckeditor" name ="short_description" id="short_description"  required>{!!$about->short_description!!} </textarea>
                                @if ($errors->has('short_description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('short_description') }}</strong>
                                    </span>
                                @endif

                            </div>


                            <div class="form-group">
                                <input class="btn btn-success" type ="submit" name ="submit" value="Save about">

                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
