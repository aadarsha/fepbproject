@extends('layouts.backmaster')



@section('content')
    <div class="container">
        <div class="row">
                <div class="col-md-8 col-lg-8 col-xl-8 col-sm-8 col-xs-8 col-lg-offset-2 col-md-offset-2 col-xs-offset-2 col-sm-offset-2 col-xl-offset-2">
                        <div class="panel panel-default">
                    <div class="panel-heading">
                        <label>Create बोर्डको गठन </label>
                        <a href="{{route('boardWork.index')}}" class="btn btn-primary pull-right">बोर्डको गठन List</a>
                    </div>

                    <div class="panel-body">
                            @if(!isset($boardWork->id))
                            <form method="post" action="{{route('boardWork.store')}}"  enctype="multipart/form-data" id="valid_form">
                                <input type ="hidden" class="form-control" name ="created_by" value="{{Auth::user()->id}}">

                                @else
                                <form method="post" action="{{route('boardWork.update',$boardWork)}}" method="post" enctype="multipart/form-data" id="valid_form">
                                        <input type="hidden" name="_method" value="put">

                                        <input type ="hidden" class="form-control" name ="updated_by" value="{{Auth::user()->id}}">
                                        <input type ="hidden" class="form-control" name ="created_by" value="{{$boardWork->created_by}}">
                             @endif
                            {{csrf_field()}}
                            {{--  <div class="form-group">
                                <label for="title">Title</label>
                                <input type ="text" class="form-control" name ="title" id="title" value="{{$boardWork->title}}" >


                            </div>  --}}

                            <div class="form-group">
                                <label>Status</label>
                                <input type ="radio"  name ="status" checked value="1">Active
                                <input type ="radio"  name ="status" value="0" > Deactive
                            </div>
                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea class="form-control ckeditor" name ="description" id="description"  required>{!!$boardWork->description!!} </textarea>
                                @if ($errors->has('description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif

                            </div>
                            <div class="form-group">
                                {{--  <span class="data-label" for="category">category</span>  --}}
                                {{--  <select class="form-control myselect" name="category" id="category" required>
                                        <option value="{{$boardWork->category}}">{{$boardWork->category}}</option>
                                        <option value="boardgathan" selected>Board Gathan</option>
                                        <option value="boardWork">Board Work</option>
                                </select>  --}}
                                <input type="hidden" class="form-control" id="category" name="category" value="boardgathan" readonly>

                        </div>
                            <div class="form-group">
                                <input class="btn btn-success" type ="submit" name ="submit" value="Save Information">

                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
