@extends('layouts.backmaster')

@section('content')
    <div class="container">
        <div class="row">
                <div class="col-md-8 col-lg-8 col-xl-8 col-sm-8 col-xs-8 col-lg-offset-2 col-md-offset-2 col-xs-offset-2 col-sm-offset-2 col-xl-offset-2">
                        <div class="panel panel-default">
                    <div class="panel-heading">
                        <label>boardWork Detail</label>
                        <a href="{{route('boardWork.index')}}" class="btn btn-primary pull-right">boardWork List</a>
                    </div>

                    <div class="panel-body">
                        <table class="table table-bordered ">
                            <tr>
                                <th>Title</th>
                                <td>{{$boardWork->title}}</td>
                            </tr>




                            <tr>
                                <th>Status</th>
                                <td>
                                    @if($boardWork->status==1)
                                        <label class="label label-success">Active</label>
                                    @else
                                        <label class="label label-danger">Deactive</label>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th>Description</th>
                                <td>{!!$boardWork->description!!}</td>
                            </tr>
                            <tr>
                                <th>Category</th>
                                <td>{!!$boardWork->category!!}</td>
                            </tr>
                            <tr>
                                <th>Created By</th>
                                <td>{{\App\User::find($boardWork->created_by)->name}}</td>
                            </tr>
                            <tr>
                                <th>Updated By</th>
                                <td>
                                    @if(!empty($boardWork->updated_by))
                                        {{\App\User::find($boardWork->updated_by)->name}}
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th>Created At</th>
                                <td>{{$boardWork->created_at}}</td>
                            </tr>
                            <tr>
                                <th>Updated At</th>
                                <td>{{$boardWork->updated_at}}</td>
                            </tr>

                            <tr>
                                <td>
                                    <a href="{{route('boardWork.edit',$boardWork)}}" class="btn btn-warning">Edit boardWork</a>
                                </td>
                                <td>
                                    <form action="{{route('boardWork.destroy',$boardWork)}}" method="post" onsubmit="return confirm('Are You Sure??')">
                                        <input type="hidden" name="_method" value="delete">
                                        {{csrf_field()}}
                                        <input type="submit" value="Delete" class="btn btn-danger">
                                    </form>

                                </td>
                            </tr>
                        </table>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

