@extends('layouts.backmaster')

@section('content')
    <div class="container">
        <div class="row">
                <div class="col-md-8 col-lg-8 col-xl-8 col-sm-8 col-xs-8 col-lg-offset-2 col-md-offset-2 col-xs-offset-2 col-sm-offset-2 col-xl-offset-2">
                        <div class="panel panel-default">
                    <div class="panel-heading">
                        <label>स्लाइडर Detail</label>
                        <a href="{{route('slider.index')}}" class="btn btn-primary pull-right"> स्लाइडर List</a>
                    </div>

                    <div class="panel-body">
                        <table class="table table-bordered ">
                            <tr>
                                <th>Title</th>
                                <td>{{$slider->title}}</td>
                            </tr>

                            <tr>
                                <th>Image</th>
                                <td><img src="{{asset('/slider_upload/'.$slider->image)}}" height="100" width="100"></td>
                            </tr>
                            <tr>
                                <th>Rank</th>
                                <td>{{$slider->rank}}</td>
                            </tr>


                            <tr>
                                <th>Status</th>
                                <td>
                                    @if($slider->status==1)
                                        <label class="label label-success">Active</label>
                                    @else
                                        <label class="label label-danger">Deactive</label>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th>Description</th>
                                <td>{{$slider->description}}</td>
                            </tr>
                            <tr>
                                <th>Created By</th>
                                <td>{{\App\User::find($slider->created_by)->name}}</td>
                            </tr>
                            <tr>
                                <th>Updated By</th>
                                <td>
                                    @if(!empty($slider->updated_by))
                                        {{\App\User::find($slider->updated_by)->name}}
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th>Created At</th>
                                <td>{{$slider->created_at}}</td>
                            </tr>
                            <tr>
                                <th>Updated At</th>
                                <td>{{$slider->updated_at}}</td>
                            </tr>

                            <tr>
                                <td>
                                    <a href="{{route('slider.edit',$slider)}}" class="btn btn-warning">Edit Slider</a>
                                </td>
                                <td>
                                    <form action="{{route('slider.destroy',$slider)}}" method="post">
                                        <input type="hidden" name="_method" value="delete">
                                        {{csrf_field()}}
                                        <input type="submit" value="Delete" class="btn btn-danger">
                                    </form>

                                </td>
                            </tr>
                        </table>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

