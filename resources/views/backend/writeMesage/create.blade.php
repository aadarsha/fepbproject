@extends('layouts.backmaster')

@section('content')
    <div class="container">
        <div class="row">
                <div class="col-md-8 col-lg-8 col-xl-8 col-sm-8 col-xs-8 col-lg-offset-2 col-md-offset-2 col-xs-offset-2 col-sm-offset-2 col-xl-offset-2">
                        <div class="panel panel-default">
                    <div class="panel-heading">
                        <label>Create </label>
                        <a href="{{route('writeMesage.index')}}" class="btn btn-primary pull-right"> List</a>
                    </div>

                    <div class="panel-body">
                            @if(!isset($writeMesage->id))
                            <form method="post" action="{{route('writeMesage.store')}}"  enctype="multipart/form-data" id="valid_form">

                                @else
                                <form method="post" action="{{route('writeMesage.update',$writeMesage)}}" method="post" enctype="multipart/form-data" id="valid_form">
                                        <input type="hidden" name="_method" value="put">
   @endif
                            {{csrf_field()}}
                            <div class="form-group">
                                    <label for="subject">Subject</label>
                                    <input type ="text" class="form-control" name ="subject" id="subject" value="{{$writeMesage->subject}}" required>
                                    @if ($errors->has('subject'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('subject') }}</strong>
                                        </span>
                                    @endif

                                </div>
                            <div class="form-group">
                                    <label for="message">message</label>
                                    <textarea class="form-control ckeditor" name ="message" id="message"  required>{!!$writeMesage->message!!} </textarea>


                                </div>



                            <div class="form-group">
                                <input class="btn btn-success" type ="submit" name ="submit" value="Save Mesage">

                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
