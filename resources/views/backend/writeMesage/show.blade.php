@extends('layouts.backmaster')

@section('content')
    <div class="container">
        <div class="row">
                <div class="col-md-8 col-lg-8 col-xl-8 col-sm-8 col-xs-8 col-lg-offset-2 col-md-offset-2 col-xs-offset-2 col-sm-offset-2 col-xl-offset-2">
                        <div class="panel panel-default">
                    <div class="panel-heading">
                        <label>writeMesage Detail</label>
                        <a href="{{route('writeMesage.index')}}" class="btn btn-primary pull-right">writeMesage List</a>
                    </div>

                    <div class="panel-body">
                        <table class="table table-bordered ">
                            <tr>
                                <th>subject</th>
                                <td>{{$writeMesage->subject}}</td>
                            </tr>




                          
                            <tr>
                                <th>message</th>
                                <td>{{$writeMesage->message}}</td>
                            </tr>

                           
                            <tr>
                                <th>Created At</th>
                                <td>{{$writeMesage->created_at}}</td>
                            </tr>
                            <tr>
                                <th>Updated At</th>
                                <td>{{$writeMesage->updated_at}}</td>
                            </tr>

                            <tr>
                                <td>
                                    <a href="{{route('writeMesage.edit',$writeMesage)}}" class="btn btn-warning">Edit writeMesage</a>
                                </td>
                                <td>
                                    {{--  <form action="{{route('writeMesage.destroy',$writeMesage)}}" method="post">
                                        <input type="hidden" name="_method" value="delete">
                                        {{csrf_field()}}
                                        <input type="submit" value="Delete" class="btn btn-danger">
                                    </form>  --}}

                                </td>
                            </tr>
                        </table>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

