@extends('layouts.backmaster')

@section('content')
        <div class="row">
            <div class="col-md-8 col-lg-8 col-xl-8 col-sm-8 col-xs-8 col-lg-offset-2 col-md-offset-2 col-xs-offset-2 col-sm-offset-2 col-xl-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <label>Information </label>
                        {{--  <a href="{{route('product_category.index')}}" class="btn btn-primary pull-right">View List</a>  --}}
                        <a href="{{url('backend/category_index'.'/'.$single_category)}}" class="btn btn-primary pull-right">View List</a>
                    </div>

                    <div class="panel-body">
                            @if(Session::has('success'))
                            <div class="alert alert-success">
                                {{ Session::get('success') }}
                            </div>
                        @endif
                        @if(Session::has('error'))
                            <div class="alert alert-danger">
                                {{ Session::get('error') }}
                            </div>
                        @endif
                        <form action="{{route('product_category.store')}}" method="post" enctype="multipart/form-data" id="valid_form">
                            {{csrf_field()}}
                            <div class="form-group ">
                                <label for="name">Title</label>
                                <input class="form-control" type="text" name="name" id="name" required>


                            </div>
                            <div class="form-group ">
<!--                                --><?php //$category= \App\MainCategory::all();?>
                                <label for="maincategory">Information Type</label>
                                @if($single_category=='sabin')
                                <select class="form-control myselect" name="maincategory" id="maincategory">
                                    <option value="0">NONE</option>
                                    @foreach($main_category as $category)
                                            <option value="{{$category->name}}">{{$category->name}}</option>
                                    @endforeach

                                </select>
                                @else
                                <input type ="text" class="form-control" name ="maincategory" id="maincategory" value="{{$single_category}}" readonly>

                                @endif


                            </div>

                            <div class="form-group">
                                <label for="status">Status</label>
                                <input type="radio" name="status" id="status" value="1" checked>Active
                                <input type="radio" name="status" id="status" value="0"  >Inactive
                            </div>



                            <div class="form-group">
                                <!-- <label>Created By</label> -->
                                <input type ="hidden" class="form-control" name ="created_by" value="{{Auth::user()->id}}">
                            </div>

                            <div class="form-group">
                                <input class="btn btn-success" type ="submit" name ="submit" value="Save Information">

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
@endsection


