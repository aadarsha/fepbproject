@extends('layouts.backmaster')

@section('content')
    <div class="container">
        <div class="row">
                <div class="col-md-8 col-lg-8 col-xl-8 col-sm-8 col-xs-8 col-lg-offset-2 col-md-offset-2 col-xs-offset-2 col-sm-offset-2 col-xl-offset-2">
                        <div class="panel panel-default">
                    <div class="panel-heading">
                        <label> Detail</label>
                        <a href="{{route('extra.index')}}" class="btn btn-primary pull-right"> List</a>
                    </div>

                    <div class="panel-body">
                        <table class="table table-bordered ">
                            <tr>
                                <th>Title</th>
                                <td>{{$extra->title}}</td>
                            </tr>
                            <tr>
                                <th>Image</th>
                                <td><img src="{{asset('/extra_upload/'.$extra->image)}}" height="100" width="100"></td>
                            </tr>
                            <tr>
                                <th>Other File</th>
                                <td><a href="{{asset('/extra_upload/'.$extra->other_file)}}" target="_blank">{{asset('/extra_upload/'.$extra->other_file)}}</a></td>
                            </tr>



                            <tr>
                                <th>Status</th>
                                <td>
                                    @if($extra->status==1)
                                        <label class="label label-success">Active</label>
                                    @else
                                        <label class="label label-danger">Deactive</label>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th>Description</th>
                                <td>{!!$extra->description!!}</td>
                            </tr>
                            <tr>
                                <th>Short Description</th>
                                <td>{!!$extra->short_description!!}</td>
                            </tr>
                            <tr>
                                <th>Created By</th>
                                <td>{{\App\User::find($extra->created_by)->name}}</td>
                            </tr>
                            <tr>
                                <th>Updated By</th>
                                <td>
                                    @if(!empty($extra->updated_by))
                                        {{\App\User::find($extra->updated_by)->name}}
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <th>Created At</th>
                                <td>{{$extra->created_at}}</td>
                            </tr>
                            <tr>
                                <th>Updated At</th>
                                <td>{{$extra->updated_at}}</td>
                            </tr>

                            <tr>
                                <td>
                                    <a href="{{route('extra.edit',$extra)}}" class="btn btn-warning">Edit extra</a>
                                </td>
                                <td>
                                    <form action="{{route('extra.destroy',$extra)}}" method="post">
                                        <input type="hidden" name="_method" value="delete">
                                        {{csrf_field()}}
                                        <input type="submit" value="Delete" class="btn btn-danger">
                                    </form>

                                </td>
                            </tr>
                        </table>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

