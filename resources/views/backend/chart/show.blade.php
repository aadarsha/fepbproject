@extends('layouts.backmaster')

@section('content')
    <div class="container">
        <div class="row">
                <div class="col-md-8 col-lg-8 col-xl-8 col-sm-8 col-xs-8 col-lg-offset-2 col-md-offset-2 col-xs-offset-2 col-sm-offset-2 col-xl-offset-2">
                        <div class="panel panel-default">
                    <div class="panel-heading">
                        <label>chart Detail</label>
                        <a href="{{route('chart.index')}}" class="btn btn-primary pull-right">chart List</a>
                    </div>

                    <div class="panel-body">
                        <tr>
                            <th>Chart Name :</th>
                            <td>
                                    {{\App\ProductCategory::find($category_id)->name}}
                            </td>
                        </tr>
                        <table class="table table-bordered ">
                            
                            <thead>
                                <th>Label</th>
                                {{-- <th>Male</th> --}}
                                {{-- <th>Female</th> --}}
                                <th>Value</th>
                                <th>Action</th>
                            </thead>
                            <tbody>
                                @foreach ($chart as $item)
                                <tr>                                      
                                        <td>{{$item->fiscal}}</td>
                                        {{-- <td>{{$item->male}}</td> --}}
                                        {{-- <td>{{$item->female}}</td> --}}
                                        <td>{{$item->total}}</td>
                                        <td><form action="{{route('chart.destroy',$item)}}" method="post">
                                                <input type="hidden" name="_method" value="delete">
                                                {{csrf_field()}}
                                                <input type="submit" value="Delete" class="btn btn-danger">
                                            </form> 
                                        </td>
                                    </tr>
                                    
                            {{-- <tr>
                                    <th>Status</th>
                                    <td>
                                        @if($item->status==1)
                                            <label class="label label-success">Active</label>
                                        @else
                                            <label class="label label-danger">Deactive</label>
                                        @endif
                                    </td>
                                </tr> --}}
                                {{-- <tr>
                                        <th>Created By</th>
                                        <td>{{\App\User::find($item->created_by)->name}}</td>
                                    </tr>
                                    <tr>
                                        <th>Updated By</th>
                                        <td>
                                            @if(!empty($chart->updated_by))
                                                {{\App\User::find($item->updated_by)->name}}
                                            @endif
                                        </td>
                                    </tr> --}}
                                    
                                    
                                @endforeach
                               

                            </tbody>
                            {{-- <tr>
                                <th>Title</th>
                                <td>{{$chart->title}}</td>
                            </tr> --}}
                            
                            {{-- <tr>
                                <th>Description</th>
                                <td>{!!$chart->description!!}</td>
                            </tr> --}}

                       
                            {{-- <tr>
                                <th>Created At</th>
                                <td>{{$chart->created_at}}</td>
                            </tr>
                            <tr>
                                <th>Updated At</th>
                                <td>{{$chart->updated_at}}</td>
                            </tr> --}}

                            <tr>
                                <td>
                                    <a href="{{route('chart.edit',$item)}}" class="btn btn-warning">Edit chart</a>
                                </td>
                                <td>
                                    {{--  <form action="{{route('chart.destroy',$chart)}}" method="post">
                                        <input type="hidden" name="_method" value="delete">
                                        {{csrf_field()}}
                                        <input type="submit" value="Delete" class="btn btn-danger">
                                    </form>  --}}

                                </td>
                            </tr>
                        </table>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

