@extends('adminlte::page')
@section('title', 'Send Mail')

@section('content_header')
    <h1>Send MAil</h1>
@stop

@section('content')

<form action="{{route('send.mail')}}" method="post" enctype="multipart/form-data" >

{{ csrf_field() }}

<div class="form-group">

<label>Subject:</label><input type="text" id="subject" name="subject" required>

</div>

<div class="form-group">

<label>Name:</label><input type="text" id="name" name="name" required>

</div>

<div class="form-group">

<label>Email:</label><input type="email" id="email" name="email" required>

</div>

<div class="form-group">

<label>MEssage:</label><textarea id="message" name="message"></textarea>

</div>

<div class="form-group">

<label for="image">Image</label>

<input type="file" name="image" id="image" class="form-control">

</div>

<input type="submit" value="Send Mail" class="btn btn-success">

</form>

@endsection
