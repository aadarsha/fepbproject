<?php

namespace App\Http\Controllers;

use App\WriteMesage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class WriteMesageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $writeMesage=WriteMesage::all();
        return view('backend.writeMesage.index',compact('writeMesage'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $writeMesage = new WriteMesage();
        return view('backend.writeMesage.create',compact('writeMesage'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input=$request->all();
        $status=WriteMesage::create($input);

        if($status){
            Session::flash('success','Information added successfully.');
        }else{
            Session::flash('error','Information cannot be added.');
        }

        return redirect('backend/writeMesage');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\WriteMesage  $writeMesage
     * @return \Illuminate\Http\Response
     */
    public function show(WriteMesage $writeMesage)
    {
        return view('backend.writeMesage.show',compact('writeMesage'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\WriteMesage  $writeMesage
     * @return \Illuminate\Http\Response
     */
    public function edit(WriteMesage $writeMesage)
    {
        return view('backend.writeMesage.create',compact('writeMesage'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\WriteMesage  $writeMesage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WriteMesage $writeMesage)
    {
        $input = $request->all();
        $status=$writeMesage->update($input);
        if($status){
            Session::flash('success','Information Updated successfully.');
        }else{
            Session::flash('error','Information Cannot be Update');
        }
        return redirect('backend/writeMesage');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\WriteMesage  $writeMesage
     * @return \Illuminate\Http\Response
     */
    public function destroy(WriteMesage $writeMesage)
    {
        $status=$writeMesage->delete();
        if($status){
            Session::flash('success','Information deleted successfully.');
        }else{
            Session::flash('error','Information cannot be deleted.');
        }
        return redirect('backend/writeMesage');
    }
}
