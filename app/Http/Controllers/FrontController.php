<?php

namespace App\Http\Controllers;

use App\Bank;
use App\Blog;
use App\About;
use App\Chart;
use App\Extra;
use App\Neyog;
use App\Banner;
use App\Member;
use App\Simple;
use App\Slider;
use App\Contact;
use App\Setting;
use App\Gapanapa;
use App\Training;
use App\BoardWork;
use App\Insurance;
use App\MobileApp;
use App\JanaGunaso;
use App\Multimedia;
use App\Description;
use App\Frontmember;
use App\OpenCountry;
use App\Orientation;
use App\Subscription;
use App\ImportantLink;
use App\YojanaKaryakram;
use App\BoardSachibalaya;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;


class FrontController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function language($lang)
    {
        // $lang = 'de'; //this value should dynamic
        App::setLocale($lang);
        return $this->index();
    }
  
    public function contactstore(Request $request)
    {
        $status=Contact::create([
            'name'=>$request->input('name'),
            'email'=>$request->input('email'),
            'subject'=>$request->input('subject'),
            'message'=>$request->input('message'),
        ]);
        if($status){
            Session::flash('success','Thank You For Suggestion. Keep in Touch');
        }else{
            Session::flash('error','Information and message cannot be added.');
        }
        return view('frontend.lastpage');
    }
    public function gunasostore(Request $request)
    {
        $input=$request->all();
        $image='';
        if(!empty($request->file('image'))){
            $file =$request->file('image');
            $path=base_path().'/public/janaGunaso_upload';
            $name=uniqid().'_'.$file->getClientOriginalName();
            if($file->move($path,$name)){
                $image=$name;
                $input['image']=$image;
            }
        }
        $status=JanaGunaso::create($input);

        if($status){
            Session::flash('success','तपाईंको अमूल्य सुझाव तथा गुनासोको लागि धन्यवाद');
        }else{
            Session::flash('error','तपाईंको अमूल्य सुझाव तथा गुनासोको लागि धन्यवाद');
        }
        // return redirect('backend/janaGunaso');
        return view('frontend.lastpage');
    }
    public function chart()
    {
        // $data = new Banner();
        $label=['sa','sn','sz','bx'];
        $value=[100,300,200,400];
        // dd($label);

        return view('frontend.chart',compact('label','value'));
    }
    public function chart2()
    {
        // $data = new Banner();
        $label=['sa','sn','sz','bx'];
        $value=[180,300,200,400];
        // dd($label);

        return view('frontend.chart2',compact('label','value'));
    }
    public function chart3()
    {
        // $data = new Banner();
        $label=['sa','sn','sz','bx'];
        $value=[80,300,200,400];
        // dd($label);

        return view('frontend.chart3',compact('label','value'));
    }
    public function index($lang=null)
    {
        $label=array();
        $value=array();
        App::setLocale($lang);
        // $sliders=Slider::where('status','1')->orderBy('id','desc')->take(2)->get();
        $banners=Banner::where('status','1')->orderBy('id','desc')->take(3)->get();
        $sliders=Slider::where('status','1')->orderBy('id','desc')->get();
        $about=About::where('status','1')->orderBy('id','desc')->take(1)->first();
        $news=Blog::where('status','1')->where('category','samachar')->orderBy('id','desc')->take(3)->get();
        $notices=Blog::where('status','1')->where('category','suchana')->orderBy('id','desc')->take(3)->get();
        $presses=Blog::where('status','1')->where('category','press')->orderBy('id','desc')->take(3)->get();
        $bills=Blog::where('status','1')->where('category','bill')->orderBy('id','desc')->take(3)->get();
        // $reports=Extra::where('status','1')->where('category','report')->where('filetype','progress')->orderBy('id','desc')->get();
        // $publications=Extra::where('status','1')->where('category','report')->where('filetype','publication')->orderBy('id','desc')->get();
    //  dd($banners);
        $publications=Extra::where('status','1')->where('category','prakashan')->orderBy('id','desc')->get();
        $reports=Extra::where('status','1')->where('category','pratibedhan')->orderBy('id','desc')->get();      

        $videos=Multimedia::where('status','1')->where('category','video')->orderBy('id','desc')->get();
        $members=Frontmember::where('status','1')->orderBy('rank','asc')->take(3)->get();
        // $frontchart=Chart::where('category','index')->get();
       
        // dd($label);

        $date=array();
        $category_id=array();
      
       

       
        $frontchart=Chart::where('status','1')->where('category','index')->orderBy('id','desc')->get(['date', 'fiscal','total','category_id']);
        foreach($frontchart as $item)
        {
            // dd($item->fiscal);
            array_push($label,$item->fiscal);
            array_push($value,$item->total);
        }
        foreach($frontchart as $item)
        {
            if(!(in_array($item->date,$date)))
            {
                array_push($date,$item->date);

            }
            if(!(in_array($item->category_id,$category_id)))
            {
                array_push($category_id,$item->category_id);

            }
            
        }
      
 


        return view('frontend.index',compact('banners','sliders','about','news','notices','presses','bills','members','reports','videos','publications','label','value','frontchart','date','category_id'));

    }


    // NAXA 
    public function geomap(Request $request)
    {
        $data=[];
        $data['insurancedata'] =Insurance::select('ID', 'CompanyName', 'Address', 'Contact', 'Email', 'Website', 'Status', 'Remark', 'latitude', 'longitude')
        ->get();
        $data['orientation'] =Orientation::select('ID', 'Permission', 'OrName', 'Address', 'Contact', 'Email', 'Website', 'Status', 'Remark', 'latitude', 'longitude')
        ->get();
        $data['orientationcount'] = count($data['orientation']);
        $data['insurancecount'] = count($data['insurancedata']);
        $data['keywordssearchs'] = $request->get('keywords-search');
        return view('frontend.geomap.index', compact('data'));
    }

    public function insurance(Request $request)
    {
        $data = array();
        $data['insurancedata'] =Insurance::select('insurance.ID','insurance.Local_Government','insurance.District','p.name as Province', 'insurance.CompanyName', 'insurance.Address', 'insurance.Contact', 'insurance.Email', 'insurance.Website', 'insurance.Status', 'insurance.Remark', 'insurance.latitude', 'insurance.longitude')
         ->leftjoin('province as p', 'p.id', '=','insurance.Province')
        ->where(function ($query) use($request) {
            if($request->has('insurance.CompanyName')) {
                $query->where('insurance.CompanyName','like','%'.$request->get('insurance.CompanyName').'%');  
            }
        })
        ->get();
       // dd(\DB::getQueryLog());
        $data['insurancecount'] = count($data['insurancedata']);
        foreach($data['insurancedata'] as $data){
            $features_report[]= array(
                "type" =>"Feature",
                "properties"=>$data,
                "geometry"=>array(
                    'type'=>'Point',
                    'coordinates'=>array(
                        $data['longitude'],
                        $data['latitude'],
                        1.0
                    ),
                ),
            );
        }
        $map_report= array(
            'name' => 'insurance',
            'type' => 'FeatureCollection',
            'features' => $features_report,
        );
        $data['insurance_map_layer']= print_r(json_encode($map_report, JSON_NUMERIC_CHECK));   
    }
    public function orientation(Request $request)
    {   $data = array();
        $data['insurance'] =Orientation::select('orientation_done.ID','orientation_done.Local_Government','orientation_done.District','p.name as Province','orientation_done.Permission', 'orientation_done.OrName', 'orientation_done.Address', 'orientation_done.Contact', 'orientation_done.Email', 'orientation_done.Website', 'orientation_done.Status', 'orientation_done.Remark', 'orientation_done.latitude', 'orientation_done.longitude')
        ->leftjoin('province as p', 'p.id', '=','orientation_done.Province')
        ->get();
        //dd(\DB::getQueryLog()); 
        $data['orientationcount'] = count($data['insurance']);
        //dd($data['orientation']);
        foreach($data['insurance'] as $data){
            $features_report[]= array(
                "type" =>"Feature",
                "properties"=>$data,
                "geometry"=>array(
                    'type'=>'Point',
                    'coordinates'=>array(
                        $data['longitude'],
                        $data['latitude'],
                        1.0
                    ),
                ),
            );
        }
        $map_report= array(
            'name' => 'orientation_centers',
            'type' => 'FeatureCollection',
            'features' => $features_report,
        );
        $data['orientation_map_layer']= print_r(json_encode($map_report, JSON_NUMERIC_CHECK));
       // dd($data['orientation_map_layer']);
    }
    public function district()
    {
        $data = array();
        $data['distict'] =Gapanapa::select('gapanapa.id', 'gapanapa.district', 'gapanapa.province','gapanapa.gapanapa','p.name as Province')
        ->leftjoin('province as p', 'p.id', '=','gapanapa.province')
        ->get();
        $data['distict']= print_r(json_encode($data['distict'], JSON_NUMERIC_CHECK));
    }

    public function datatable()
    {
        return view('frontend.geomap.datatable', compact('data'));
    }


    // NAXA 


    public function aboutus()
    {
        $boardgathans=BoardWork::where('status','1')->where('category','boardgathan')->orderBy('id','desc')->take(1)->get();
        $boardworks=BoardWork::where('status','1')->where('category','boardwork')->orderBy('id','desc')->take(1)->get();
        $boardsachabalayas=BoardSachibalaya::where('status','1')->get();
        // $multimediacategory=DB::table('product_categories')->where('maincategory','Other')->get();
        $sachibalaya=Simple::orderBy('id','desc')->where('category','boardsachawalya')->take(1)->first();
        $gathan=Simple::orderBy('id','desc')->where('category','boardgathan')->take(1)->first();
        $boardwork=Simple::orderBy('id','desc')->where('category','boardwork')->take(1)->first();
        // $shortcontents=DB::table('simples')->distinct()->get();
        // dd($shortcontents);
        $about=About::where('status','1')->orderBy('id','desc')->take(1)->first();
        return view('frontend.about',compact('about','boardworks','boardgathans','boardsachabalayas','sachibalaya','gathan','boardwork'));

    }
    public function yojanakaryakram()
    {
        $yojanas=YojanaKaryakram::where('category','yojana')->orderBy('id','desc')->get();
        $karyakrams=YojanaKaryakram::where('category','karyakram')->orderBy('id','desc')->get();
        return view('frontend.yojanakaryakram',compact('yojanas','karyakrams'));
    }
    public function media()
    {
        $videos=Blog::where('category','video')->get();
        $photos=Blog::where('category','photo')->get();
        // dd($photos);
        // dd($photos->unique('title'));
        $presses=Blog::where('category','press')->get();
        $bills=Blog::where('category','bill')->get();
        $suchanas=Blog::where('category','suchana')->get();
        return view('frontend.media',compact('videos','photos','presses','bills','suchanas'));
    }
    public function download()
    {
        // $reports=Extra::where('status','1')->where('category','report')->where('filetype','progress')->orderBy('id','desc')->get();
        // $publications=Extra::where('status','1')->where('category','report')->where('filetype','publication')->orderBy('id','desc')->get();
        $publications=Extra::where('status','1')->where('category','report')->orderBy('id','desc')->get();
        $ains=Extra::where('status','1')->where('category','ain')->orderBy('id','desc')->get();
        $bulletins=Extra::where('status','1')->where('category','bulletin')->orderBy('id','desc')->get();
        $prakashans=Extra::where('status','1')->where('category','prakashan')->orderBy('id','desc')->get();
        $pragatis=Extra::where('status','1')->where('category','pratibedhan')->where('filetype','pragati prathibedhan')->orderBy('id','desc')->get();
        $adhyans=Extra::where('status','1')->where('category','pratibedhan')->where('filetype','adhyan prathibedhan')->orderBy('id','desc')->get();
        $others=Extra::where('status','1')->where('category','other')->orderBy('id','desc')->get();
        $nebadans=Extra::where('status','1')->where('category','nebadan')->orderBy('id','desc')->get();
        $bills=Blog::where('status','1')->where('category','bill')->orderBy('id','desc')->get();

        return view('frontend.download',compact('pragatis','adhyans','publications','ains','bulletins','others','nebadans','prakashans','bills'));
    }
    public function pratibedhantype($type=null)
    {
        // dd($type);
        // $reports=Extra::where('status','1')->where('category','report')->where('filetype','progress')->orderBy('id','desc')->get();
        // $publications=Extra::where('status','1')->where('category','report')->where('filetype','publication')->orderBy('id','desc')->get();
        $publications=Extra::where('status','1')->where('category','report')->orderBy('id','desc')->get();
        $ains=Extra::where('status','1')->where('category','ain')->orderBy('id','desc')->get();
        $bulletins=Extra::where('status','1')->where('category','bulletin')->orderBy('id','desc')->get();
        $prakashans=Extra::where('status','1')->where('category','prakashan')->orderBy('id','desc')->get();
        if(isset($type))
        {
            $pragatis=Extra::where('status','1')->Where('short_description',$type)->where('category','pratibedhan')->where('filetype','pragati prathibedhan')->orderBy('id','desc')->get();

        }
        else
        {
            $pragatis=Extra::where('status','1')->where('category','pratibedhan')->where('filetype','pragati prathibedhan')->orderBy('id','desc')->get();

        }
        $adhyans=Extra::where('status','1')->where('category','pratibedhan')->where('filetype','adhyan prathibedhan')->orderBy('id','desc')->get();
        $others=Extra::where('status','1')->where('category','other')->orderBy('id','desc')->get();
        $nebadans=Extra::where('status','1')->where('category','nebadan')->orderBy('id','desc')->get();

        return view('frontend.pratibedhantype',compact('pragatis','adhyans','publications','ains','bulletins','others','nebadans','prakashans'));
    }
    public function photocollection($category)
    {

        // dd($category);
        $photos=Blog::where('category','photo')->where('title',$category)->get();
        // dd($photos);

        return view('frontend.photocollection',compact('photos'));


    }
    public function detailpage($id,$category)
    {
        // dd($id.'+'.$category);
        switch ($category)
        {
            case 'suchana':
                $data = Blog::find($id);
                // dd($data);
            case 'samachar':
                $data = Blog::find($id);
            case 'press':
                $data = Blog::find($id);

            case 'bill':
                $data = Blog::find($id);

        }


        return view('frontend.detailpage',compact('data','id','category'));

    }


    public function contact()
    {
        $setting=Setting::orderBy('id','desc')->take(1)->first();
        // dd($settings);
        // foreach($settings as $s)
        // {
        //     dd($s->address);
        // }

        return view('frontend.contact',compact('setting'));
    }
    public function walefaredetail($category_id)
    {
        // dd($category_id);
        $walefares=Description::where('status','1')->where('category_id',$category_id)->orderBy('id','desc')->get();
        // dd($walefares);
        return view('frontend.walefaredetail',compact('walefares'));
        

    }
    public function walefare()
    {
        $walefares=array();
        $category=Description::select('category_id')->distinct()->get();
        foreach ($category as $key => $item) {
            $tmpdesc=Description::where('status','1')->where('category_id',$item->category_id)->orderBy('id','desc')->get();
            array_push($walefares,$tmpdesc);
            
        }
        
        return view('frontend.walefare',compact('walefares'));

    }
    public function boardkaryafinal()
    {
                    $labeltmp=array();
                    // $tmplabel=array();
                    $valuetmp=array();
                    $value=array();
                    $label=array();
                    // $tmpvalue=array();
                    $charts=array();
                    $trainings=Training::where('status','1')->orderBy('id','desc')->get();
                    $category=Chart::select('category_id')->distinct()->get();
                    // dd($category);
                    foreach ($category as $key => $item) {
                        $tmpcharts=Chart::where('status','1')->where('category_id',$item->category_id)->where('category','boardkarya')->orderBy('id','desc')->get();
                        array_push($charts,$tmpcharts);
                        # code...
                    }
                    

                    foreach ($charts as $i => $items) {
                        $tmplabel=array();
                        $tmpvalue=array();
                    
                        foreach ($items as $key => $item) {
                            array_push($tmplabel,$item->fiscal);
                            array_push($tmpvalue,$item->total);

                        
                        }
                        array_push($labeltmp,$tmplabel);

                        array_push($valuetmp,$tmpvalue);
                    
                        

                    }

                
                
            for($i=0;$i<count($labeltmp);$i++)
            {
                for($j=0;$j<count($labeltmp[$i]);$j++)
                {
                
                    array_push($label,$labeltmp[$i][$j]);


                }

            }


            for($i=0;$i<count($valuetmp);$i++)
            {
                for($j=0;$j<count($valuetmp[$i]);$j++)
                {
                
                    array_push($value,$valuetmp[$i][$j]);


                }

            }


                    return view('frontend.boardkaryafinal',compact('trainings','charts','labeltmp','valuetmp'));
                }
   
    public function boardkarya()
    {
  
     
        $date=array();
        $category_id=array();
      
        $trainings=Training::where('status','1')->orderBy('id','desc')->get();
             
    

       
        $chart=Chart::where('status','1')->where('category','boardkarya')->orderBy('id','desc')->get(['date', 'fiscal','total','category_id']);
        foreach($chart as $item)
        {
            if(!(in_array($item->date,$date)))
            {
                array_push($date,$item->date);

            }
            if(!(in_array($item->category_id,$category_id)))
            {
                array_push($category_id,$item->category_id);

            }
            
        }
      
 
        
        

        return view('frontend.boardkarya',compact('trainings','chart','date','category_id'));
    }
    public function rojgarprocess()
    {
        return view('frontend.rojgarprocess');
    }
    public function mobileapp()
    {
        $mobile=MobileApp::orderBy('id','desc')->take(1)->first();
        // dd($mobile);
        return view('frontend.mobileapp',compact('mobile'));
    }
    public function tathyankabibaran()
    {
        return view('frontend.tathyankabibaran');
    }
    public function samrachana()
    {
        $members=Member::orderBy('rank','asc')->get();
        return view('frontend.samrachana',compact('members'));
    }
    public function memberdetail($id)
    {
        // dd($id);
        $member=Member::find($id);
        return view('frontend.memberdetail',compact('member'));
    }
    public function chetanamulak()
    {
        $videos=Multimedia::where('status','1')->where('category','video')->orderBy('id','desc')->get();
        $audios=Multimedia::where('status','1')->where('category','audio')->orderBy('id','desc')->get();
        $photos=Multimedia::where('status','1')->where('category','photo')->orderBy('id','desc')->get();
        $infographic=Simple::orderBy('id','desc')->where('category','infographic')->take(1)->first();
        $faqs=Multimedia::where('status','1')->where('category','faq')->orderBy('id','desc')->get();
        $institutes=Multimedia::where('status','1')->where('category','institute')->orderBy('id','desc')->get();
        $basicnotes=Multimedia::where('status','1')->where('category','basicnote')->orderBy('id','desc')->take(1)->get();
        $links=ImportantLink::where('status',1)->orderBy('id','desc')->get();
        $banks=Bank::where('status',1)->orderBy('id','desc')->get();
        $neyogs=Neyog::where('status',1)->orderBy('id','desc')->get();
        $openCountries=OpenCountry::where('status',1)->orderBy('id','asc')->get();

        return view('frontend.chetanamulak',compact('videos','audios','photos','infographic','faqs','institutes','basicnotes','links','banks','neyogs','openCountries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
