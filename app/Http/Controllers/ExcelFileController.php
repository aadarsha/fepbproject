<?php

namespace App\Http\Controllers;

use File;
use Excel;
// use Maatwebsite\Excel\Excel;
// use Maatwebsite\Excel\Facades\Excel;
use Session;
use App\ExcelFile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class ExcelFileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function importExport()
    {
        return view('importExport');
    }
    public function import(Request $request){
        //validate the xls file
        $this->validate($request, array(
            'file'      => 'required'
        ));

        if($request->hasFile('file')){
            $extension = File::extension($request->file->getClientOriginalName());
            if ($extension == "xlsx" || $extension == "xls" || $extension == "csv") {

                $path = $request->file->getRealPath();
                $data = Excel::load($path, function($reader) {
                })->get();
                if(!empty($data) && $data->count()){

                    foreach ($data as $key => $value) {
                        // dd($value);
                        $insert[] = [
                        'title' => $value->title,
                        'description' => $value->description,
                        'date' => $value->date,
                        ];
                    }

                    if(!empty($insert)){

                        $insertData = DB::table('excel_files')->insert($insert);
                        if ($insertData) {
                            Session::flash('success', 'Your Data has successfully imported');
                        }else {
                            Session::flash('error', 'Error inserting the data..');
                            return back();
                        }
                    }
                }

                return back();

            }else {
                Session::flash('error', 'File is a '.$extension.' file.!! Please upload a valid xls/csv file..!!');
                return back();
            }
        }
    }
    public function downloadExcel($type)
    {
        // $data = ExcelFile::select('title', 'description', 'date')->get()->toArray();

        $columns = DB::connection()->getSchemaBuilder()->getColumnListing("excel_files");

        // dd($columns);

        // $data=array('title','address','phone','description');

        // $data=$columns;
        $data=array();
        for($i=0;$i<sizeof($columns);$i++)
        {
            if($columns[$i]!='id'&&$columns[$i]!='created_at'&&$columns[$i]!='updated_at')
            {
                array_push($data,$columns[$i]);
            }

        }
// dd($data);
                return Excel::create('Database', function($excel) use ($data) {

            $excel->sheet('mySheet', function($sheet) use ($data)
            {
                $sheet->fromArray($data);
            });
        })->download($type);
    }
    public function importExcel(Request $request)
    {
        $request->validate([
            'import_file' => 'required'
        ]);

        $path = $request->file('import_file')->getRealPath();
        $data = Excel::load($path)->get();

        if($data->count()){
            foreach ($data as $key => $value) {
                $arr[] = ['title' => $value->title, 'description' => $value->description];
            }

            if(!empty($arr)){
                ExcelFile::insert($arr);
            }
        }

        return back()->with('success', 'Insert Record successfully.');
    }
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ExcelFile  $excelFile
     * @return \Illuminate\Http\Response
     */
    public function show(ExcelFile $excelFile)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ExcelFile  $excelFile
     * @return \Illuminate\Http\Response
     */
    public function edit(ExcelFile $excelFile)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ExcelFile  $excelFile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ExcelFile $excelFile)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ExcelFile  $excelFile
     * @return \Illuminate\Http\Response
     */
    public function destroy(ExcelFile $excelFile)
    {
        //
    }
}
