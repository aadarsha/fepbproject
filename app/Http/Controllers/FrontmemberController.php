<?php

namespace App\Http\Controllers;

use App\Frontmember;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class FrontmemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $frontmember=Frontmember::orderBy('id','desc')->get();
        return view('backend.frontmember.index',compact('frontmember'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $frontmember = new Frontmember();
        $frontmemberold = Frontmember::all();
        return view('backend.frontmember.create',compact('frontmember','frontmemberold'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input=$request->all();

        $image='';
        if(!empty($request->file('image'))){
            $file =$request->file('image');
            $path=base_path().'/public/frontmember_upload';
            $name=uniqid().'_'.$file->getClientOriginalName();
            if($file->move($path,$name)){
                $image=$name;
                $input['image']=$image;
            }
        }
        $status=Frontmember::create($input);

        if($status){
            Session::flash('success','Information added successfully.');
        }else{
            Session::flash('error','Information cannot be added.');
        }

        return redirect('backend/frontmember');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Frontmember  $frontmember
     * @return \Illuminate\Http\Response
     */
    public function show(Frontmember $frontmember)
    {
        return view('backend.frontmember.show',compact('frontmember'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Frontmember  $frontmember
     * @return \Illuminate\Http\Response
     */
    public function edit(Frontmember $frontmember)
    {
        $frontmemberold = Frontmember::all();
        return view('backend.frontmember.create',compact('frontmember','frontmemberold'));
 

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Frontmember  $frontmember
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Frontmember $frontmember)
    {
        //
        $input = $request->all();
        // dd($request);
        $image='';
        if(!empty($request->file('image'))){
            $file =$request->file('image');
            $path=base_path().'/public/frontmember_upload';
            $name=uniqid().'_'.$file->getClientOriginalName();
            if($file->move($path,$name)){
                $image=$name;
                $input['image']=$image;

            }
        }
        // dd($input);

        $status=$frontmember->update($input);
        if($status){
            Session::flash('success','Information Updated successfully.');
        }else{
            Session::flash('error','Information Cannot be Update');
        }
        return redirect('backend/frontmember');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Frontmember  $frontmember
     * @return \Illuminate\Http\Response
     */
    public function destroy(Frontmember $frontmember)
    {
        //
        // $frontmember=Member::find($id);
        $status=$frontmember->delete();
        if($status){
            Session::flash('success','member deleted successfully.');
        }else{
            Session::flash('error','member cannot be deleted.');
        }
        return redirect('backend/frontmember');
    }
}
