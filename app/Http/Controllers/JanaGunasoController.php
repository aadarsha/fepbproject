<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Session;



use App\JanaGunaso;
use Illuminate\Http\Request;

class JanaGunasoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $janaGunaso=JanaGunaso::orderBy('id','desc')->get();
        return view('backend.janaGunaso.index',compact('janaGunaso'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $janaGunaso = new JanaGunaso();
        return view('backend.janaGunaso.create',compact('janaGunaso'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input=$request->all();
        $image='';
        if(!empty($request->file('image'))){
            $file =$request->file('image');
            $path=base_path().'/public/janaGunaso_upload';
            $name=uniqid().'_'.$file->getClientOriginalName();
            if($file->move($path,$name)){
                $image=$name;
                $input['image']=$image;
            }
        }
        $status=JanaGunaso::create($input);

        if($status){
            Session::flash('success','Information and message saved successfully.');
        }else{
            Session::flash('error','Information and message cannot be added.');
        }
        return redirect('backend/janaGunaso');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\JanaGunaso  $janaGunaso
     * @return \Illuminate\Http\Response
     */
    public function show(JanaGunaso $janaGunaso)
    {
        return view('backend.janaGunaso.show',compact('janaGunaso'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\JanaGunaso  $janaGunaso
     * @return \Illuminate\Http\Response
     */
    public function edit(JanaGunaso $janaGunaso)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\JanaGunaso  $janaGunaso
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, JanaGunaso $janaGunaso)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\JanaGunaso  $janaGunaso
     * @return \Illuminate\Http\Response
     */
    public function destroy(JanaGunaso $janaGunaso)
    {
        $status=$janaGunaso->delete();
        if($status){
            Session::flash('success','janaGunaso deleted successfully.');
        }else{
            Session::flash('error','janaGunaso cannot be deleted.');
        }
        return redirect('backend/janaGunaso');
    }
}
