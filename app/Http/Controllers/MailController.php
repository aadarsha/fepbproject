<?php

namespace App\Http\Controllers;

use App\WriteMesage;
use App\Subscription;
use App\Mail\SendMailable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class MailController extends Controller
{

    public function writemail()
    {
        return view('email.mailsend');

    }
    public function subscription(Request $request)
    {
        // dd($request);
      
        $email=$request->input('email');
        $name=$myArray = explode('@', $email);
        $name=$name[0];
        $status=Subscription::create([
            'email'=>$request->input('email'),
            
        ]);
        if($status){
            Session::flash('success','Thank You For Suggestion. Keep in Touch');
        }else{
            Session::flash('error','Information and message cannot be added.');
        }
        return $this->maildirect($email,$name);
        // return view('frontend.lastpage');

    }
    public function maildirect($email,$name)

    {
        $docs='';
        $messagesdata=WriteMesage::first();
              
        // $name=$request->input('name');
        $subject=$messagesdata->subject;
        $to=$email;
        $msg=$messagesdata->message;
        $message=$name.','.$msg;
       Mail::to($to)->send(new SendMailable($message,$subject,$docs));
       return back()->withErrors($name.' Thank you !!!');
    //    return redirect()->back()->withInput($name);
        // return view('email.confirm',compact('name'));
    }
    public function mail(Request $request)

    {
        $docs='';
        if(!empty($request->file('image')))
        {
            $file=$request->file('image');
            $path=base_path().'/public/document_upload';
            $name=uniqid().'_'.$file->getClientOriginalName();
            if($file->move($path,$name))
            {
                $docs=$name;
                $docs=base_path().'\public\document_upload'.'\\'.$docs;
            }

        }
        $name=$request->input('name');
        $subject=$request->input('subject');
        $to=$request->input('email');
        $msg=$request->input('message');
        $message=$name.','.$msg;




       Mail::to($to)->send(new SendMailable($message,$subject,$docs));

       
        return view('email.confirm',compact('name'));
    }
}
