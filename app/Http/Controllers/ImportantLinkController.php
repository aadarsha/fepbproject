<?php

namespace App\Http\Controllers;

use App\ImportantLink;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ImportantLinkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $importantLink=ImportantLink::orderBy('id','desc')->get();
        return view('backend.importantLink.index',compact('importantLink'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $importantLink = new ImportantLink();
        return view('backend.importantLink.create',compact('importantLink'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input=$request->all();

        $status=ImportantLink::create($input);

        if($status){
            Session::flash('success','Information added successfully.');
        }else{
            Session::flash('error','Information cannot be added.');
        }

        return redirect('backend/importantLink');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ImportantLink  $importantLink
     * @return \Illuminate\Http\Response
     */
    public function show(ImportantLink $importantLink)
    {
        return view('backend.importantLink.show',compact('importantLink'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ImportantLink  $importantLink
     * @return \Illuminate\Http\Response
     */
    public function edit(ImportantLink $importantLink)
    {
        return view('backend.importantLink.create',compact('importantLink'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ImportantLink  $importantLink
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ImportantLink $importantLink)
    {
        $input = $request->all();
        // dd($request);

        $status=$importantLink->update($input);
        if($status){
            Session::flash('success','Information Updated successfully.');
        }else{
            Session::flash('error','Information Cannot be Update');
        }
        return redirect('backend/importantLink');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ImportantLink  $importantLink
     * @return \Illuminate\Http\Response
     */
    public function destroy(ImportantLink $importantLink)
    {
        $status=$importantLink->delete();
        if($status){
            Session::flash('success','Information deleted successfully.');
        }else{
            Session::flash('error','Information cannot be deleted.');
        }
        return redirect('backend/importantLink');
    }
}
