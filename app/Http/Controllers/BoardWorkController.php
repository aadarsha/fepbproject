<?php

namespace App\Http\Controllers;

use App\BoardWork;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class BoardWorkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $boardWork=BoardWork::orderBy('id','desc')->get();
        // $boardWork=boardWork::where('category','boardwork')->get();
        return view('backend.boardWork.index',compact('boardWork'));
    }
    public function boardindex($category)
    {
        // $boardWork=boardWork::orderBy('id','desc')->get();
        $boardWork=BoardWork::where('category',$category)->orderBy('id','desc')->get();
        return view('backend.boardWork.index',compact('boardWork'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $boardWork = new BoardWork();
        return view('backend.boardWork.create',compact('boardWork'));

    }
    public function creategathan()
    {

        $boardWork = new BoardWork();
        return view('backend.boardWork.creategathan',compact('boardWork'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input=$request->all();

        $status=BoardWork::create($input);

        if($status){
            Session::flash('success','Information added successfully.');
        }else{
            Session::flash('error','Information cannot be added.');
        }

        return redirect('backend/boardWork');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BoardWork  $boardWork
     * @return \Illuminate\Http\Response
     */
    public function show(BoardWork $boardWork)
    {
        return view('backend.boardWork.show',compact('boardWork'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BoardWork  $boardWork
     * @return \Illuminate\Http\Response
     */
    public function edit(BoardWork $boardWork)
    {
        if($boardWork->category=='boardgathan')
        {
            return view('backend.boardWork.creategathan',compact('boardWork'));
                }
        elseif($boardWork->category=='boardWork')
        {
            return view('backend.boardWork.create',compact('boardWork'));
          }
          else{
                          return view('backend.boardWork.create',compact('boardWork'));

          }


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BoardWork  $boardWork
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BoardWork $boardWork)
    {
        $input = $request->all();

        $status=$boardWork->update($input);
        if($status){
            Session::flash('success','Information Updated successfully.');
        }else{
            Session::flash('error','Information Cannot be Update');
        }
        return redirect('backend/boardWork');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BoardWork  $boardWork
     * @return \Illuminate\Http\Response
     */
    public function destroy(BoardWork $boardWork)
    {
        // dd($boardWork);
        $status=$boardWork->delete();
        if($status){
            Session::flash('success','Information deleted successfully.');
        }else{
            Session::flash('error','Information cannot be deleted.');
        }
        return redirect('backend/boardWork');
    }
}
