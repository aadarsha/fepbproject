<?php

namespace App\Http\Controllers;

use App\Simple;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class SimpleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $simple=Simple::orderBy('id','desc')->get();
        return view('backend.simple.index',compact('simple'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $simple = new Simple();
        return view('backend.simple.create',compact('simple'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input=$request->all();

        $status=Simple::create($input);

        if($status){
            Session::flash('success','Information added successfully.');
        }else{
            Session::flash('error','Information cannot be added.');
        }

        return redirect('backend/simple');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Simple  $simple
     * @return \Illuminate\Http\Response
     */
    public function show(Simple $simple)
    {
        return view('backend.simple.show',compact('simple'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Simple  $simple
     * @return \Illuminate\Http\Response
     */
    public function edit(Simple $simple)
    {
        return view('backend.simple.create',compact('simple'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Simple  $simple
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Simple $simple)
    {

            $input = $request->all();
            // dd($request);

            $status=$simple->update($input);
            if($status){
                Session::flash('success','Information Updated successfully.');
            }else{
                Session::flash('error','Information Cannot be Update');
            }
            return redirect('backend/simple');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Simple  $simple
     * @return \Illuminate\Http\Response
     */
    public function destroy(Simple $simple)
    {
        $status=$simple->delete();
        if($status){
            Session::flash('success','Information deleted successfully.');
        }else{
            Session::flash('error','Information cannot be deleted.');
        }
        return redirect('backend/simple');

    }
}
