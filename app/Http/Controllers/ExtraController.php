<?php

namespace App\Http\Controllers;

use App\Extra;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class ExtraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $extra=Extra::orderBy('id','desc')->get();
        return view('backend.extra.index',compact('extra'));
    }
    public function downloadindex($category)
    {

        $extra=Extra::where('category',$category)->orderBy('id','desc')->get();
        return view('backend.extra.index',compact('extra'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $extra = new Extra();
        $extracategory=DB::table('product_categories')->where('maincategory','Other')->get();
        return view('backend.extra.create',compact('extra','extracategory'));
    }
    public function ain()
    {
        $extra = new Extra();
        $extracategory=DB::table('product_categories')->where('maincategory','Other')->get();
        return view('backend.extra.ain',compact('extra','extracategory'));
    }

    public function bulletin()
    {
        $extra = new Extra();
        $extracategory=DB::table('product_categories')->where('maincategory','Other')->get();
        return view('backend.extra.bulletin',compact('extra','extracategory'));
    }
    public function prakashan()
    {
        $extra = new Extra();
        $extracategory=DB::table('product_categories')->where('maincategory','Other')->get();
        return view('backend.extra.prakashan',compact('extra','extracategory'));
    }
    public function pratibedhan()
    {
        $extra = new Extra();
        $extracategory=DB::table('product_categories')->where('maincategory','Other')->get();
        return view('backend.extra.pratibedhan',compact('extra','extracategory'));
    }
    public function report()
    {
        $extra = new Extra();
        $extracategory=DB::table('product_categories')->where('maincategory','Other')->get();
        return view('backend.extra.report',compact('extra','extracategory'));
    }
    public function other()
    {
        $extra = new Extra();
        $extracategory=DB::table('product_categories')->where('maincategory','Other')->get();
        return view('backend.extra.other',compact('extra','extracategory'));
    }
    public function nebadan()
    {
        $extra = new Extra();
        $extracategory=DB::table('product_categories')->where('maincategory','Other')->get();
        return view('backend.extra.nebadan',compact('extra','extracategory'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input=$request->all();

        $image='';
        if(!empty($request->file('image'))){
            $file =$request->file('image');
            $path=base_path().'/public/extra_upload';
            $name=uniqid().'_'.$file->getClientOriginalName();
            if($file->move($path,$name)){
                $image=$name;
                $input['image']=$image;
            }
        }
        $other_file='';
        if(!empty($request->file('other_file'))){
            $file =$request->file('other_file');
            $path=base_path().'/public/extra_upload';
            $name=uniqid().'_'.$file->getClientOriginalName();
            if($file->move($path,$name)){
                $other_file=$name;
                $input['other_file']=$other_file;

            }
        }
        $status=Extra::create($input);

        if($status){
            Session::flash('success','Information added successfully.');
        }else{
            Session::flash('error','Information cannot be added.');
        }

        return redirect('backend/extra');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Extra  $extra
     * @return \Illuminate\Http\Response
     */
    public function show(Extra $extra)
    {
        return view('backend.extra.show',compact('extra'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Extra  $extra
     * @return \Illuminate\Http\Response
     */
    public function edit(Extra $extra)
    {
        $extracategory=DB::table('product_categories')->where('maincategory','Other')->get();
        if($extra->category=='ain')
        {
            return view('backend.extra.ain',compact('extra','extracategory'));
        }
        elseif($extra->category=='bulletin')
        {
            return view('backend.extra.bulletin',compact('extra','extracategory'));
        }
        elseif($extra->category=='basicnote')
        {
            return view('backend.extra.basicnote',compact('extra','extracategory'));
        }
        elseif($extra->category=='faq')
        {
            return view('backend.extra.faq',compact('extra','extracategory'));
        }
        elseif($extra->category=='institute')
        {
            return view('backend.extra.institute',compact('extra','extracategory'));
        }
        elseif($extra->category=='prakashan')
        {
            return view('backend.extra.prakashan',compact('extra','extracategory'));
        }
        elseif($extra->category=='pratibedhan')
        {
            return view('backend.extra.pratibedhan',compact('extra','extracategory'));
        }
        elseif($extra->category=='report')
        {
            return view('backend.extra.report',compact('extra','extracategory'));
        }
        elseif($extra->category=='other')
        {
            return view('backend.extra.other',compact('extra','extracategory'));
        }
        elseif($extra->category=='nebadan')
        {
            return view('backend.extra.nebadan',compact('extra','extracategory'));
        }
        else
        return view('backend.extra.create',compact('extra','extracategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Extra  $extra
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Extra $extra)
    {
        $input = $request->all();
        // dd($request);
        $image='';
        if(!empty($request->file('image'))){
            $file =$request->file('image');
            $path=base_path().'/public/extra_upload';
            $name=uniqid().'_'.$file->getClientOriginalName();
            if($file->move($path,$name)){
                $image=$name;
                $input['image']=$image;

            }
        }
        $other_file='';
        if(!empty($request->file('other_file'))){
            $file =$request->file('other_file');
            $path=base_path().'/public/extra_upload';
            $name=uniqid().'_'.$file->getClientOriginalName();
            if($file->move($path,$name)){
                $other_file=$name;
                $input['other_file']=$other_file;

            }
        }
        // dd($input);

        $status=$extra->update($input);
        if($status){
            Session::flash('success','Information Updated successfully.');
        }else{
            Session::flash('error','Information Cannot be Update');
        }
        return redirect('backend/extra');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Extra  $extra
     * @return \Illuminate\Http\Response
     */
    public function destroy(Extra $extra)
    {
        $status=$extra->delete();
        if($status){
            Session::flash('success','Information deleted successfully.');
        }else{
            Session::flash('error','Information cannot be deleted.');
        }
        return redirect('backend/extra');
    }
}
