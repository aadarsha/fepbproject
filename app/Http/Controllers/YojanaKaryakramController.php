<?php

namespace App\Http\Controllers;

use App\YojanaKaryakram;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class YojanaKaryakramController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $yojanaKaryakram=YojanaKaryakram::all();
        return view('backend.yojanaKaryakram.index',compact('yojanaKaryakram'));
    }
    public function yojanaindex($category)
    {
        $yojanaKaryakram=YojanaKaryakram::where('category',$category)->orderBy('id','desc')->get();
        return view('backend.yojanaKaryakram.index',compact('yojanaKaryakram'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $yojanaKaryakram = new YojanaKaryakram();
        return view('backend.yojanaKaryakram.create',compact('yojanaKaryakram'));
    }
    public function createkaryakram()
    {
        $yojanaKaryakram = new YojanaKaryakram();
        return view('backend.yojanaKaryakram.karyakram',compact('yojanaKaryakram'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input=$request->all();

        $image='';
        if(!empty($request->file('image'))){
            $file =$request->file('image');
            $path=base_path().'/public/yojanaKaryakram_upload';
            $name=uniqid().'_'.$file->getClientOriginalName();
            if($file->move($path,$name)){
                $image=$name;
                $input['image']=$image;
            }
        }
        $other_file='';
        if(!empty($request->file('other_file'))){
            $file =$request->file('other_file');
            $path=base_path().'/public/yojanaKaryakram_upload';
            $name=uniqid().'_'.$file->getClientOriginalName();
            if($file->move($path,$name)){
                $other_file=$name;
                $input['other_file']=$other_file;

            }
        }
        $status=YojanaKaryakram::create($input);

        if($status){
            Session::flash('success','Information added successfully.');
        }else{
            Session::flash('error','Information cannot be added.');
        }

        return redirect('backend/yojanaKaryakram');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\YojanaKaryakram  $yojanaKaryakram
     * @return \Illuminate\Http\Response
     */
    public function show(YojanaKaryakram $yojanaKaryakram)
    {
        return view('backend.yojanaKaryakram.show',compact('yojanaKaryakram'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\YojanaKaryakram  $yojanaKaryakram
     * @return \Illuminate\Http\Response
     */
    public function edit(YojanaKaryakram $yojanaKaryakram)
    {
        if ($yojanaKaryakram->category=='yojana') {
            return view('backend.yojanaKaryakram.create',compact('yojanaKaryakram'));

        }
        else {
            return view('backend.yojanaKaryakram.karyakram',compact('yojanaKaryakram'));
        }


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\YojanaKaryakram  $yojanaKaryakram
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, YojanaKaryakram $yojanaKaryakram)
    {
        $input = $request->all();
        // dd($request);
        $image='';
        if(!empty($request->file('image'))){
            $file =$request->file('image');
            $path=base_path().'/public/yojanaKaryakram_upload';
            $name=uniqid().'_'.$file->getClientOriginalName();
            if($file->move($path,$name)){
                $image=$name;
                $input['image']=$image;

            }
        }
        $other_file='';
        if(!empty($request->file('other_file'))){
            $file =$request->file('other_file');
            $path=base_path().'/public/yojanaKaryakram_upload';
            $name=uniqid().'_'.$file->getClientOriginalName();
            if($file->move($path,$name)){
                $other_file=$name;
                $input['other_file']=$other_file;

            }
        }
        // dd($input);

        $status=$yojanaKaryakram->update($input);
        if($status){
            Session::flash('success','Information Updated successfully.');
        }else{
            Session::flash('error','Information Cannot be Update');
        }
        return redirect('backend/yojanaKaryakram');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\YojanaKaryakram  $yojanaKaryakram
     * @return \Illuminate\Http\Response
     */
    public function destroy(YojanaKaryakram $yojanaKaryakram)
    {
             $status=$yojanaKaryakram->delete();
        if($status){
            Session::flash('success','Information deleted successfully.');
        }else{
            Session::flash('error','Information cannot be deleted.');
        }
        return redirect('backend/yojanaKaryakram');
    }
}
