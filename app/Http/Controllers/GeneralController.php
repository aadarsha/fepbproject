<?php

namespace App\Http\Controllers;

use App\General;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class GeneralController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $general=General::orderBy('id','desc')->get();
        return view('backend.general.index',compact('general'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $general = new General();
        $categories=DB::table('product_categories')->where('maincategory','Board Welfare Work')->orderBy('id','desc')->get();
        return view('backend.general.create',compact('general','categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input=$request->all();

        $status=General::create($input);

        if($status){
            Session::flash('success','Information added successfully.');
        }else{
            Session::flash('error','Information cannot be added.');
        }

        return redirect('backend/general');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\General  $general
     * @return \Illuminate\Http\Response
     */
    public function show(General $general)
    {
        return view('backend.general.show',compact('general'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\General  $general
     * @return \Illuminate\Http\Response
     */
    public function edit(General $general)
    {
        // $categories=DB::table('product_categories')->orderBy('id','desc')->get();
        $categories=DB::table('product_categories')->where('maincategory','Board Welfare Work')->orderBy('id','desc')->get();
        return view('backend.general.create',compact('general','categories'));;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\General  $general
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, General $general)
    {

            $input = $request->all();
            // dd($request);

            $status=$general->update($input);
            if($status){
                Session::flash('success','Information Updated successfully.');
            }else{
                Session::flash('error','Information Cannot be Update');
            }
            return redirect('backend/general');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\General  $general
     * @return \Illuminate\Http\Response
     */
    public function destroy(General $general)
    {
        $status=$general->delete();
        if($status){
            Session::flash('success','Information deleted successfully.');
        }else{
            Session::flash('error','Information cannot be deleted.');
        }
        return redirect('backend/general');
    }
}
