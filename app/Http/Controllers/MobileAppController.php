<?php

namespace App\Http\Controllers;

use App\MobileApp;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class MobileAppController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mobileApp=MobileApp::all();
        return view('backend.mobileApp.index',compact('mobileApp'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $mobileApp = new MobileApp();
        return view('backend.mobileApp.create',compact('mobileApp'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input=$request->all();

        $image='';
        if(!empty($request->file('image'))){
            $file =$request->file('image');
            $path=base_path().'/public/mobileApp_upload';
            $name=uniqid().'_'.$file->getClientOriginalName();
            if($file->move($path,$name)){
                $image=$name;
                $input['image']=$image;
            }
        }
        $mobile_image='';
        if(!empty($request->file('mobile_image'))){
            $file =$request->file('mobile_image');
            $path=base_path().'/public/mobileApp_upload';
            $name=uniqid().'_'.$file->getClientOriginalName();
            if($file->move($path,$name)){
                $mobile_image=$name;
                $input['mobile_image']=$mobile_image;

            }
        }
        $status=MobileApp::create($input);

        if($status){
            Session::flash('success','Information added successfully.');
        }else{
            Session::flash('error','Information cannot be added.');
        }

        return redirect('backend/mobileApp');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MobileApp  $mobileApp
     * @return \Illuminate\Http\Response
     */
    public function show(MobileApp $mobileApp)
    {
        return view('backend.mobileApp.show',compact('mobileApp'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MobileApp  $mobileApp
     * @return \Illuminate\Http\Response
     */
    public function edit(MobileApp $mobileApp)
    {
        return view('backend.mobileApp.create',compact('mobileApp'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MobileApp  $mobileApp
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MobileApp $mobileApp)
    {
        $input = $request->all();
        // dd($request);
        $image='';
        if(!empty($request->file('image'))){
            $file =$request->file('image');
            $path=base_path().'/public/mobileApp_upload';
            $name=uniqid().'_'.$file->getClientOriginalName();
            if($file->move($path,$name)){
                $image=$name;
                $input['image']=$image;

            }
        }
        $mobile_image='';
        if(!empty($request->file('mobile_image'))){
            $file =$request->file('mobile_image');
            $path=base_path().'/public/mobileApp_upload';
            $name=uniqid().'_'.$file->getClientOriginalName();
            if($file->move($path,$name)){
                $mobile_image=$name;
                $input['mobile_image']=$mobile_image;

            }
        }
        // dd($input);

        $status=$mobileApp->update($input);
        if($status){
            Session::flash('success','Information Updated successfully.');
        }else{
            Session::flash('error','Information Cannot be Update');
        }
        return redirect('backend/mobileApp');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MobileApp  $mobileApp
     * @return \Illuminate\Http\Response
     */
    public function destroy(MobileApp $mobileApp)
    {
        $status=$mobileApp->delete();
        if($status){
            Session::flash('success','Information deleted successfully.');
        }else{
            Session::flash('error','Information cannot be deleted.');
        }
        return redirect('backend/mobileApp');
    }
}
