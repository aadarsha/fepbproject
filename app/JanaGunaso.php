<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JanaGunaso extends Model
{
    protected $table='jana_gunasos';
    protected $fillable=[
        'name',
        'email',
        'phone',
        'address',
        'message',
        'image',
        ];
}
