<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MobileApp extends Model
{
    protected $table='mobile_apps';
    protected $fillable=[
        'title',
        'status',
        'description',
        'link',
        'image',
        'mobile_image',
        'short_description',
        'created_by',
        'updated_by'
    ];
}
