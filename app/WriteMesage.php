<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WriteMesage extends Model
{
    protected $table='write_mesages';
    protected $fillable=[
        'subject',      
        'message',      
    ];
}
