<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    protected $table='banners';
    protected $fillable=[
        'title',
        'status',
        'description',
        'date',
        'image',
        'link',
        'created_by',
        'updated_by'
    ];
}
