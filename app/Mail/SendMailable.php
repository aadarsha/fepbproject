<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMailable extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $name;
    public $subject;
    public $docs;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($message,$subject,$docs)
    {
        $this->name = $message;
        $this->subject = $subject;
        $this->docs = $docs;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if(!empty($this->docs))
        {

            return $this->attach($this->docs)->subject($this->subject)->view('email.sendedmessage');
        }
        else
        {
            return $this->subject($this->subject)->view('email.sendedmessage');


        }

    }

}
