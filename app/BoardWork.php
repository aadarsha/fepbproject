<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BoardWork extends Model
{
    protected $table='board_works';
    protected $fillable=[
        'title',
        'status',
        'category',
        'description',
        'created_by',
        'updated_by'
    ];
}
