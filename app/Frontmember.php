<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Frontmember extends Model
{
        
    protected $table='frontmembers';
    protected  $fillable=['name','description','address','phone','email','position','parent','rank','joined_from','status','image','created_by','updated_by'];

}
