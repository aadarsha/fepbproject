<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Insurance extends Model
{ 
   protected $table = 'insurance';
   protected $fillable = ['ID', 'CompanyName', 'Address', 'Contact', 'Email', 'Website', 'Status', 'Remark', 'latitude', 'longitude'];
}
