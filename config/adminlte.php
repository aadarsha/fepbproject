<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Title
    |--------------------------------------------------------------------------
    |
    | The default title of your admin panel, this goes into the title tag
    | of your page. You can override it per page with the title section.
    | You can optionally also specify a title prefix and/or postfix.
    |
    */

    'title' => 'Admin Pannel',

    'title_prefix' => '',

    'title_postfix' => '',

    /*
    |--------------------------------------------------------------------------
    | Logo
    |--------------------------------------------------------------------------
    |
    | This logo is displayed at the upper left corner of your admin panel.
    | You can use basic HTML here if you want. The logo has also a mini
    | variant, used for the mini side bar. Make it 3 letters or so
    |
    */

    'logo' => '<b>MOEWRWS </b>ADMIN',

    'logo_mini' => '<b>FE</b>PB',

    /*
    |--------------------------------------------------------------------------
    | Skin Color
    |--------------------------------------------------------------------------
    |
    | Choose a skin color for your admin panel. The available skin colors:
    | blue, black, purple, yellow, red, and green. Each skin also has a
    | ligth variant: blue-light, purple-light, purple-light, etc.
    |
    */

    'skin' => 'black',

    /*
    |--------------------------------------------------------------------------
    | Layout
    |--------------------------------------------------------------------------
    |
    | Choose a layout for your admin panel. The available layout options:
    | null, 'boxed', 'fixed', 'top-nav'. null is the default, top-nav
    | removes the sidebar and places your menu in the top navbar
    |
    */

    'layout' => null,

    /*
    |--------------------------------------------------------------------------
    | Collapse Sidebar
    |--------------------------------------------------------------------------
    |
    | Here we choose and option to be able to start with a collapsed side
    | bar. To adjust your sidebar layout simply set this  either true
    | this is compatible with layouts except top-nav layout option
    |
    */

    'collapse_sidebar' => false,

    /*
    |--------------------------------------------------------------------------
    | URLs
    |--------------------------------------------------------------------------
    |
    | Register here your dashboard, logout, login and register URLs. The
    | logout URL automatically sends a POST request in Laravel 5.3 or higher.
    | You can set the request to a GET or POST with logout_method.
    | Set register_url to null if you don't want a register link.
    |
    */

    'dashboard_url' => 'home',

    'logout_url' => 'logout',

    'logout_method' => null,

    'login_url' => 'login',

    'register_url' => 'register',

    /*
    |--------------------------------------------------------------------------
    | Menu Items
    |--------------------------------------------------------------------------
    |
    | Specify your menu items to display in the left sidebar. Each menu item
    | should have a text and and a URL. You can also specify an icon from
    | Font Awesome. A string instead of an array represents a header in sidebar
    | layout. The 'can' is a filter on Laravel's built in Gate functionality.
    |
    */

    'menu' => [
        // 'MAIN NAVIGATION',
        // [
        //     'text' => 'Blog',
        //     'url'  => 'admin/blog',
        //     'can'  => 'manage-blog',
        // ],
        // [
        //     'text'        => 'Pages',
        //     'url'         => 'admin/pages',
        //     'icon'        => 'file',
        //     'label'       => 4,
        //     'label_color' => 'success',
        // ],
        [
            'text'=>'Write Mail',
            'url'=>'/write/mail',
            'icon'=>'file',

        ],
        'BACKEND',
        [
            'text' => 'सेटिंग',
            'url'  => 'backend/setting',
            'icon' => 'gear',
        ],
        [
            'text' => 'गृह पृष्ठ स्लाइडर',
            'url'  => 'backend/slider',
            'icon' => 'sliders',
        ],
        [
            'text'=> 'हाम्रो बारेमा',
            'icon'=>'users',
            'submenu'=>[
                [
                    'text' =>'बोर्डको परिचय',
                    'icon_color' => 'red',
                    'url'=>'backend/about'
                ],
                [
                    'text'=>'बोर्डको गठन',
                    'icon_color' => 'blue',
                    'url'=>'backend/board/index/boardgathan'
                ],
                [
                    'text'=>'बोर्डको काम',
                    'icon_color' => 'yellow',
                    'url'=>'backend/board/index/boardWork'
                ],
                [
                    'text'=>'बोर्डको सचिवालय',
                    'icon_color' => 'green',
                    'url'=>'backend/boardSachibalaya'
                ],
                [
                    'text'=>'संगठन संरचना',
                    'icon_color' => 'orange',
                    'url'=>'#'
                ],


            ]

        ],
        [
            'text' => 'Services',
            'icon' => 'edit',
            'submenu'=>[
                // [
                //     'text' =>'Services',
                //     'icon_color' => 'red',
                //     'url'  => 'backend/general/create',
                // ],
                [
                    'text' =>'Description',
                    'icon_color' => 'green',
                    'url'  => 'backend/description/create',
                ],
                [
                    'text' => 'Walefare Category',
                    'url'  => 'backend/create_category/Board Welfare Work',
                    'icon_color' => 'red',
                ],
                // [
                //     'text' =>'Table Name',
                //     'icon_color' => 'yellow',
                //     'url'  => 'backend/manualTable',
                // ],
                // [
                //     'text' =>'Table Field',
                //     'icon_color' => 'blue',
                //     'url'  => 'backend/tableEntry',
                // ],
            ]
        ],

        // [
        //     'text' => 'Service Category',
        //     'url'  => 'backend/product_category/create',
        //     'icon' => 'edit',
        // ],


        [
            'text'=> 'योजना तथा कार्यक्रम ',
            'icon'=>'sitemap',
            'submenu'=>[
                [
                    'text' =>'योजना',
                    'icon_color' => 'red',
                    'url'=>'backend/yojanaandkaryakram/index/yojana'
                ],
                [
                    'text'=>'कार्यक्रम',
                    'icon_color' => 'blue',
                    'url'=>'backend/yojanaandkaryakram/index/karyakram'
                ],

            ]

        ],
        [
            'text'=> 'मिडिया ',
            'icon'=>'newspaper-o',
            'submenu'=>[
                [
                    'text' =>'सूचना तथा समाचारहरु',
                    'icon_color' => 'red',
                    // 'url'=>'backend/suchana/create'
                    'url'=>'backend/media_index/suchana'
                ],
                [
                    'text'=>'प्रेस विज्ञप्ति',
                    'icon_color' => 'blue',
                    // 'url'=>'backend/press/create'
                    'url'=>'backend/media_index/press'
                ],
                [
                    'text'=>'विल सार्वजनिकरण',
                    'icon_color' => 'blue',
                    // 'url'=>'backend/bill/create'
                    'url'=>'backend/media_index/bill'

                ],
                [
                    'text'=>'तस्वीर संग्रह',
                    'icon_color' => 'yellow',
                    // 'url'=>'backend/gallery/create'
                    'url'=>'backend/media_index/photo'

                ],
                [
                    'text' => 'तस्विर संग्रह Name',
                    'url'  => 'backend/create_category/Album',
                    'icon_color' => 'red',
                ],
                [
                    'text'=>'भिडीयो संग्रह',
                    'icon_color' => 'black',
                    // 'url'=>'backend/video/create'
                    'url'=>'backend/media_index/video'

                ],


            ]

        ],
        [
            'text'=> 'डाउनलोड',
            'icon'=>'download',
            'submenu'=>[
                [
                    'text' =>' ऐन तथा नियमावली',
                    'icon_color' => 'red',
                    // 'url'=>'backend/ain/create'
                    'url'=>'backend/download/ain'

                ],
                [
                    'text'=>'बुलेटिन',
                    'icon_color' => 'blue',
                    // 'url'=>'backend/bulletin/create'
                    'url'=>'backend/download/bulletin'
                ],
                [
                    'text'=>'प्रकाशन',
                    'icon_color' => 'red',
                    // 'url'=>'backend/bulletin/create'
                    'url'=>'backend/download/prakashan'
                ],
                [
                    'text'=>'प्रगति प्रतिवेदन',
                    'icon_color' => 'yellow',
                    // 'url'=>'backend/pratibedhan/create'
                    'url'=>'backend/download/pratibedhan'
                ],
                [
                    'text'=>' रिपोर्ट',
                    'icon_color' => 'green',
                    // 'url'=>'backend/report/create'
                    'url'=>'backend/download/report'
                ],
                [
                    'text'=>' अन्य डाउनलोड्स',
                    'icon_color' => 'gray',
                    // 'url'=>'backend/other/create'
                    'url'=>'backend/download/other'
                ],
                [
                    'text'=>'निवेदन फारमहरु',
                    'icon_color' => 'black',
                    // 'url'=>'backend/nebadan/create'
                    'url'=>'backend/download/nebadan'
                ],



            ]

        ],
        [
            'text'=> 'सीपमूलक तालिम',
            'icon'=>'edit',
            'submenu'=>[
                [
                    'text' => 'Dynamic Category',
                    'url'  => 'backend/create_category/Skillfull training',
                    'icon_color' => 'red',
                ],
                [
                    'text' => 'Dynamic Training',
                    'url'  => 'backend/training',
                    'icon_color' => 'blue',
                ],


            ]
            ],
        [
            'text'=> 'Chart Diagram',
            'icon'=>'edit',
            'submenu'=>[
                [
                    'text' => 'Chart Category',
                    'url'  => 'backend/create_category/chart',
                    'icon_color' => 'red',
                ],
                [
                    'text' => 'Dynamic Chart',
                    'url'  => 'backend/chart',
                    'icon_color' => 'blue',
                ],


            ]
            ],
        [
            'text'=> 'सूचनामूलक सामाग्री',
            'icon'=>'newspaper-o',
            'submenu'=>[
                [
                    'text'=>'Info Graphics',
                    'icon_color' => 'red',
                    // 'url'=>'backend/multimediaphoto/create'
                     'url'=>'backend/chetanamulak/photo'

                ],
                [
                    'text'=>' सूचनामूलक भिडीयो ',
                    'icon_color' => 'yellow',
                    // 'url'=>'backend/multimediavideo/create'
                    'url'=>'backend/chetanamulak/video'
                ],
                [
                    'text'=>'सूचनामूलक अडियो ',
                    'icon_color' => 'black',
                    // 'url'=>'backend/multimediaaudio/create'
                    'url'=>'backend/chetanamulak/audio'
                ],
                [
                    'text'=>'बारम्बार सोधिने प्रश्न',
                    'icon_color' => 'black',
                    // 'url'=>'backend/nebadan/create'
                    'url'=>'backend/chetanamulak/faq'
                ],
                [
                    'text'=>'कामदारहरुले ध्यान दिनु पर्ने कुराहरु',
                    'icon_color' => 'black',
                    // 'url'=>'backend/nebadan/create'
                    'url'=>'backend/chetanamulak/basicnote'
                ],
                [
                    'text'=>'सहयोगी संस्थाहरु',
                    'icon_color' => 'black',
                    // 'url'=>'backend/nebadan/create'
                    'url'=>'backend/chetanamulak/institute'
                ],
                [
                    'text' => 'Info Graphic Title',
                    'url'  => 'backend/simple',
                    'icon-color' => 'black',
                ],
                [
                    'text' => 'Bank ',
                    'url'  => 'backend/bank',
                    'icon-color' => 'red',
                ],
                [
                    'text' => 'Open Country ',
                    'url'  => 'backend/openCountry',
                    'icon-color' => 'green',
                ],
                [
                    'text' => 'Neyog',
                    'url'  => 'backend/neyog',
                    'icon-color' => 'yellow',
                ],


            ]

        ],
        // [
        //     'text' => 'About',
        //     'url'  => 'backend/about',
        //     'icon' => 'users',
        // ],
        // [
        //     'text' => 'Board Formation ,Work',
        //     'url'  => 'backend/boardWork',
        //     'icon' => 'sitemap',
        // ],
        [
            'text' => 'मोबाइल एप',
            'url'  => 'backend/mobileApp',
            'icon' => 'mobile-phone',
        ],
        [
            'text' => 'ब्यानर',
            'url'  => 'backend/banner',
            'icon' => 'mobile-phone',
        ],
        [
            'text' => 'महत्वपूर्ण लिङ्कहरु',
            'url'  => 'backend/importantLink',
            'icon' => 'link',
        ],
        // [
        //     'text' => 'Blogs',
        //     'url'  => 'backend/blog',
        //     'icon' => 'newspaper-o',
        // ],
        // [
        //     'text' => 'Other Materials',
        //     'url'  => 'backend/extra',
        //     'icon' => 'newspaper-o',
        // ],
        [
            'text' => 'बोर्डको सचिवालय',
            'url'  => 'backend/member',
            'icon' => 'users',
        ],
        [
            'text' => 'Show Member in Front',
            'url'  => 'backend/frontmember',
            'icon' => 'users',
        ],
        [
            'text' => 'सम्पर्क',
            'url'  => 'backend/contact',
            'icon' => 'phone-square',
        ],
        [
            'text' => 'Jana Gunaso',
            'url'  => 'backend/janaGunaso',
            'icon' => 'phone-square',
        ],
        [
            'text' => 'Message for email',
            'url'  => 'backend/writeMesage',
            'icon' => 'phone-square',
        ],
        [
            'text' => 'Subscription',
            'url'  => 'backend/subscription',
            'icon' => 'phone-square',
        ],


        // [
        //     'text' => 'Other Category',
        //     'url'  => 'backend/create_category/Other',
        //     'icon' => 'edit',
        // ],
        // [
        //     'text' => 'Base Category',
        //     'url'  => 'backend/product_category/create',
        //     'icon' => 'edit',
        // ],
        // [
        //     'text' => 'Change Password',
        //     'url'  => 'admin/settings',
        //     'icon' => 'lock',
        // ],

        // [
        //     'text'    => 'Multilevel',
        //     'icon'    => 'share',
        //     'submenu' => [
        //         [
        //             'text' => 'Level One',
        //             'url'  => '#',
        //         ],
        //         [
        //             'text'    => 'Level One',
        //             'url'     => '#',
        //             'submenu' => [
        //                 [
        //                     'text' => 'Level Two',
        //                     'url'  => '#',
        //                 ],
        //                 [
        //                     'text'    => 'Level Two',
        //                     'url'     => '#',
        //                     'submenu' => [
        //                         [
        //                             'text' => 'Level Three',
        //                             'url'  => '#',
        //                         ],
        //                         [
        //                             'text' => 'Level Three',
        //                             'url'  => '#',
        //                         ],
        //                     ],
        //                 ],
        //             ],
        //         ],
        //         [
        //             'text' => 'Level One',
        //             'url'  => '#',
        //         ],
        //     ],
        // ],
        // 'LABELS',
        // [
        //     'text'       => 'Important',
        //     'icon_color' => 'red',
        // ],
        // [
        //     'text'       => 'Warning',
        //     'icon_color' => 'yellow',
        // ],
        // [
        //     'text'       => 'Information',
        //     'icon_color' => 'aqua',
        // ],
        // 'ACCOUNT SETTINGS',
        // [
        //     'text' => 'Profile',
        //     'route' => 'admin.profile',
        //     'icon' => 'user'
        // ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Menu Filters
    |--------------------------------------------------------------------------
    |
    | Choose what filters you want to include for rendering the menu.
    | You can add your own filters to this array after you've created them.
    | You can comment out the GateFilter if you don't want to use Laravel's
    | built in Gate functionality
    |
    */

    'filters' => [
        JeroenNoten\LaravelAdminLte\Menu\Filters\HrefFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ActiveFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\SubmenuFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ClassesFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\GateFilter::class,
    ],

    /*
    |--------------------------------------------------------------------------
    | Plugins Initialization
    |--------------------------------------------------------------------------
    |
    | Choose which JavaScript plugins should be included. At this moment,
    | only DataTables is supported as a plugin. Set the value to true
    | to include the JavaScript file from a CDN via a script tag.
    |
    */

    'plugins' => [
        'datatables' => true,
        'select2'    => true,
        'chartjs'    => true,
        'dropify'=>true,
    ],
];
